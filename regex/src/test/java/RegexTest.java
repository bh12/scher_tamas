
import java.util.regex.Pattern;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RegexTest {

//    @Test
//    public void testAlma() {
//    //given
//    String alma = "alma";
//    
//    //when
//    boolean result = Pattern.matches("alma", alma);
//    
//    //then
//    Assertions.assertTrue(result);
//}
    //Pontosan text text szo legyen
//       @Test
//    public void testKalap() {
//    //given
//    String kalap = "kalap";
//    
//    //when
//    boolean result = Pattern.matches("kalap", kalap);
//    
//    //then
//    Assertions.assertTrue(result);
//}
    //Lehet “a” vagy “b” betu pontosan egyszer
//    @Test
//    public void testAB1() {
//    //given
//    String text = "a";
//    
//    //when
//    boolean result = Pattern.matches("[ab]", text);
//    
//    //then
//    Assertions.assertTrue(result);
//}
    //Barmennyi “a” vagy “b” betu lehet (nullaszor is)
//        @Test
//    public void testAB1() {
//    //given
//    String text = "abbab";
//    
//    //when
//    boolean result = Pattern.matches("[ab]*", text);
//    
//    //then
//    Assertions.assertTrue(result);
//}
    //Barmi mas lehessen benne csak “a” vagy “b” betu nem
//            @Test
//    public void testNoAB() {
//    //given
//    String text = "lmi";
//    
//    //when
//    boolean result = Pattern.matches("[^ab]*", text);
//    
//    //then
//    Assertions.assertTrue(result);
//}
    //Kezdodjon “bela”-val
//    @Test
//       public void testBeginsBela() {
//    //given
//    String text = "belabacsi";
//    
//    //when
//    boolean result = Pattern.matches("^(bela).*$", text);
//    
//    //then
//    Assertions.assertTrue(result);
//}
    //Vegzodjon “alma”-val
//        @Test
//       public void testEndsAlma() {
//    //given
//    String text = "belabacsialma";
//    
//    //when
//    boolean result = Pattern.matches("^.*(alma)$", text);
//    
//    //then
//    Assertions.assertTrue(result);
//}
    //Vegzodjon “a” vagy “b” betuvel
//               @Test
//       public void testEndsAorB() {
//    //given
//    String text = "belabacsialmb";
//    
//    //when
//    boolean result = Pattern.matches("^.*[ab]$", text);
//    
//    //then
//    Assertions.assertTrue(result);
//}
    //Tartalmazza a “hamu” szot barhol a szovegben
//                   @Test
//       public void testContainsHamu() {
//    //given
//    String text = "ahamuacsialmb";
//    
//    //when
//    boolean result = Pattern.matches("^.*(hamu).*$", text);
//    
//    //then
//    Assertions.assertTrue(result);
//}
    //Pontosan 3 darab szam legyen
//                        @Test
//       public void test3number() {
//    //given
//    String text = "120";
//    
//    //when
//    boolean result = Pattern.matches("[0-9]{3}", text); //itt lehet a //d
//    
//    //then
//    Assertions.assertTrue(result);
//}
    //Legalabb 3 darab “a” betu legyen
    @Test
    public void testMin3A() {
        //given
        String text = "aagnfaaaf";

        //when
        boolean result = Pattern.matches("^.*[a]{3,}.*$", text);

        //then
        Assertions.assertTrue(result);
    }
    
//    Maximum 5 darab “b” betu legyen
//Legyen 3-6 darab “c” betu
//1000-2999 ig fogadjon el egy adott szoveget

}
