
package threadprimecollector;

import java.util.List;


public class PrimeCollector extends Thread implements PrimeWriter{

    @Override
    public void wiritePrimeToConsole(List<PrimeNumber> primeNumbers) {
        for (PrimeNumber prim : primeNumbers) {
            System.out.println(prim.getPrimeNumber() + ", ");
        }
    }

    @Override
    public void writePrimeIntoFile(List<PrimeNumber> primeNumbers) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    List<PrimeNumber> primeNumbers;
    
    private int threadCount;
    private int totalIntervall;
    private int lengthOfAnIntervall;
    private int min;

    public PrimeCollector(List<PrimeNumber> primeNumbers, int threadCount, int totalIntervall, int min) {
        this.primeNumbers = primeNumbers;
        this.threadCount = threadCount;
        this.totalIntervall = totalIntervall;
        this.lengthOfAnIntervall = setLengthOfAnIntervall(totalIntervall, threadCount);
        this.min = min * getLengthOfAnIntervall();
    }

    public List<PrimeNumber> getPrimeNumbers() {
        return primeNumbers;
    }

    public void setPrimeNumbers(List<PrimeNumber> primeNumbers) {
        this.primeNumbers = primeNumbers;
    }

    public int getThreadCount() {
        return threadCount;
    }

    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
    }

    public int getTotalIntervall() {
        return totalIntervall;
    }

    public void setTotalIntervall(int totalIntervall) {
        this.totalIntervall = totalIntervall;
    }

    public int getLengthOfAnIntervall() {
        return lengthOfAnIntervall;
    }

    private int setLengthOfAnIntervall(int totalIntervall, int threadCount) {
        return this.lengthOfAnIntervall = totalIntervall / threadCount;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }
    
    
    
    


    @Override
    public void run() {
        
        for (int i = getMin(); i < getMin() + getLengthOfAnIntervall(); i++) {
            
            try {
                
                if (PrimeNumber.isPrime(i)) {
                PrimeNumber primeNumber = new PrimeNumber(i);
                
                synchronized (primeNumbers) {
                    primeNumbers.add(primeNumber);
                }
                }
                
            } catch (Exception e) {
                
            }
            
            
        }
       
    }
    
    
    
}
