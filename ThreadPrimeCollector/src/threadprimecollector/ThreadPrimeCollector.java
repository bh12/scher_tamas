package threadprimecollector;

import java.util.ArrayList;
import java.util.List;

public class ThreadPrimeCollector {

    public static void main(String[] args) throws InterruptedException {
        // TODO code application logic here

        List<PrimeNumber> primeNumbers = new ArrayList<>();
        List<PrimeCollector> workers = new ArrayList<>();

        int threadCount = 4;

        for (int i = 0; i < threadCount; i++) {

            PrimeCollector primeCollectorThread = new PrimeCollector(primeNumbers, threadCount, 1000, i);
            primeCollectorThread.start();
            workers.add(primeCollectorThread);
            
        }
        
        for (PrimeCollector t : workers) {
            t.join();
        }
        
        print(primeNumbers);
        
        //System.out.println(PrimeNumber.isPrime(289));

        //PrimeCollector.wr
        
    }

    public static void print(List<PrimeNumber> primeNumbers) {
        for (PrimeNumber prim : primeNumbers) {
            System.out.println(prim.getPrimeNumber() + ", ");
        }
    }

}
