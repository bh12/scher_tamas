
package threadprimecollector;

import java.util.List;


public interface PrimeWriter {
    
    void wiritePrimeToConsole(List<PrimeNumber> primeNumbers);
    void writePrimeIntoFile(List<PrimeNumber> primeNumbers);
    
}
