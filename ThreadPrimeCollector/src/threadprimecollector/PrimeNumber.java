
package threadprimecollector;


public class PrimeNumber {
    
    private int primeNumber;

    public PrimeNumber(int primeNumber) {
        this.primeNumber = primeNumber;
    }

    public int getPrimeNumber() {
        return primeNumber;
    }

    public void setPrimeNumber(int primeNumber) {
        this.primeNumber = primeNumber;
    }
    
    
    
      public static boolean isPrime(int n) {
        int divisorCounter = 0;
        for (int i = 1; i <= n/2; i++) {
            if (n % i == 0) {
                divisorCounter++;
            }
        }
        
        return divisorCounter < 3;
    }

    @Override
    public String toString() {
        return "PrimeNumber{" + "primeNumber=" + primeNumber + '}';
    }
      
      



    
}
