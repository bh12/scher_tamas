package bookreaderswingapp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.imageio.stream.FileImageInputStream;

public class Controller {

    private View view;
    private Modell modell;

    public Controller() {
        this.view = new View(this);
        this.modell = new Modell();
        this.view.init();

    }

    public void fileWriter() {
        File file = new File("textSaved.txt");
        try (FileWriter fw = new FileWriter(file);
                BufferedWriter bw = new BufferedWriter(fw)) {
            bw.write(textFromArea());
            //bw.flush();

        } catch (IOException ie) {
            System.out.println("File hiba");
            ie.getStackTrace();
        }

    }

    public String textFromArea() {
        String text;
        text = this.view.getScreen().getText();
        return text;

    }

    public String txtFileReader() {
        File file = new File("textSaved.txt");
        StringBuilder sb = new StringBuilder();
        try (FileReader fr = new FileReader(file);
                BufferedReader br = new BufferedReader(fr)) {

            String line;

            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append("\n");

            }

        } catch (IOException ie) {
            System.out.println("File hiba");
            ie.getStackTrace();

        }
        String text = sb.toString();
        return text;

    }

    public void updateTextAreafromTxtFile() {
        String text = txtFileReader();
        this.view.setScreen(text);
    }

    public void serialiseAreaText() {

        try (FileOutputStream fos = new FileOutputStream("book.ser");
                ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(textAreaToBook());

        } catch (IOException ie) {
            System.out.println("File hiba");
            ie.getStackTrace();
        }

    }

    public Book textAreaToBook() {
        Book book = new Book(textFromArea());
        return book;
    }

    public Book readSerialisedBook() {

        Book book = null;
        try (FileInputStream fis = new FileInputStream("book.ser");
                ObjectInputStream ois = new ObjectInputStream(fis)) {

            book = (Book) ois.readObject();

        } catch (IOException ie) {
            System.out.println("File hiba");
            ie.getStackTrace();

        } catch (ClassNotFoundException ce) {
            System.out.println("osztály hiba");
            ce.getStackTrace();
        }

        return book;
    }

    public void updateTextAreafromSerFile() {
        String text = readSerialisedBook().getBookContent();
        this.view.setScreen(text);
    }

}
