package bookreaderswingapp;

import java.io.Serializable;

public class Book implements Serializable {

    private String bookContent;

    public Book(String bookContent) {
        this.bookContent = bookContent;
    }

    public String getBookContent() {
        return bookContent;
    }

    public void setBookContent(String bookContent) {
        this.bookContent = bookContent;
    }

}
