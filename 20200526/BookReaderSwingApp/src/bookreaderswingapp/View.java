package bookreaderswingapp;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class View extends JFrame {

    private Controller controller;

    //private JPanel panel;  
    private JTextArea screen;
    private JButton saveButton;
    private JButton serialiseButton;
    private JButton readFile;

    public View() {
    }

    public View(Controller controller) {
        this.controller = controller;
    }

    public void init() {

        //panel = new JPanel();
        this.setSize(400, 400);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setTitle("Book handling");

        screen = new JTextArea();
        add(screen);

        saveButton = new JButton("save to txt");
        add(saveButton);
        saveButton.addActionListener(e -> controller.fileWriter());

        serialiseButton = new JButton("serialise");
        add(serialiseButton);
        serialiseButton.addActionListener(e -> controller.serialiseAreaText());

        readFile = new JButton("read (txt & ser files)");
        add(readFile);
        //readFile.addActionListener(e -> controller.updateTextAreafromTxtFile());
        readFile.addActionListener(e -> controller.updateTextAreafromSerFile());

        this.setLayout(new GridLayout(4, 0));

        this.setVisible(true);

    }

    public JTextArea getScreen() {
        return screen;
    }

    public void setScreen(String string) {
        this.screen.setText(string);
    }

}
