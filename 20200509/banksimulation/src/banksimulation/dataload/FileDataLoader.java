package banksimulation.dataload;

import banksimulation.BankApplication;
import banksimulation.model.Client;
import banksimulation.model.Transfer;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class FileDataLoader implements DataLoader {

    @Override
    public void loadInitialData() {
        File clientFile = new File("clients.txt");

        try (FileReader fr = new FileReader(clientFile);
                BufferedReader br = new BufferedReader(fr);) {
            String line = br.readLine();
            while (line != null) {
                String[] clientArray = line.split(",");

                String clientId = clientArray[0];
                String accountNumber = clientArray[1];
                long balance = Long.parseLong(clientArray[2]);

                Client c = new Client(clientId, accountNumber, balance);

                BankApplication.clients.add(c);

                line = br.readLine();
            }

         
        } catch (IOException e) {
            System.out.println("Hiba a fájl beolvasása közben! :(");
        }

        File transferFile = new File("transfers.txt");

        try (FileReader fr = new FileReader(transferFile);
                BufferedReader br = new BufferedReader(fr);) {
            String line = br.readLine();
            while (line != null) {
                String[] transferArray = line.split(",");

                String searchedSource = transferArray[0];
                List<String> clientNames = BankApplication.clients.stream()
                        .map(client -> client.getClientId())
                        .collect(Collectors.toList());
                int searchedSourceIndex = clientNames.indexOf(searchedSource);
                System.out.println(searchedSourceIndex);
                Client source = BankApplication.clients.get(searchedSourceIndex);

                String searchedTarget = transferArray[1];
                int searchedTargetIndex = clientNames.indexOf(searchedTarget);
                Client target = BankApplication.clients.get(searchedTargetIndex);

                long amount = Long.parseLong(transferArray[2]);
                Date date = new Date();

                Transfer t = new Transfer(source, target, amount, date);

                BankApplication.transfers.add(t);

                line = br.readLine();
            }
        } catch (IOException e) {
            System.out.println("Hiba a fájl beolvasása közben! :(");
        }

    }

}
