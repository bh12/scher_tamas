
package banksimulation.model;
public class Client {

    private String clientId;

    private String accountNumber;

    private long balance;
    
    private long openingBalance;
    
    private long balanceChange;

    public Client(String clientId, String accountNumber, long balance) {
        this.clientId = clientId;
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.openingBalance = balance;
    }

    public long getOpeningBalance() {
        return openingBalance;
    }

    public long getBalanceChange() {
        return balanceChange;
    }

    public void setBalanceChange(long balanceChange) {
        this.balanceChange = balanceChange;
    }
    
    
    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Client{" + "clientId=" + clientId + ", accountNumber=" + accountNumber + ", balance=" + balance + '}';
    }
    
    public long balanceChange() {
        long balanceChange = balance - openingBalance;
        
        return balanceChange;
    }
    
}
