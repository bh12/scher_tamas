
package javaapplication8;

import static java.util.Collections.list;


public class JavaApplication8 {

    public static void main(String[] args) throws CloneNotSupportedException {
     
       StartingPos pos = new StartingPos();
        System.out.println("prototípus tartalma clonozás elött");
        System.out.print(pos.getPieceList().get(0).getPosX());
        System.out.println(pos.getPieceList().get(0).getPosY());
        
        StartingPos posNew = (StartingPos)pos.clone();
        
        System.out.println("clonozott objektum tartalma");
        System.out.print(posNew.getPieceList().get(0).getPosX());
        System.out.println(posNew.getPieceList().get(0).getPosY());
        posNew.getPieceList().get(0).setPosX('b');
        
        System.out.println("clonozot objektum értékének megváltoztatása");
        System.out.print(posNew.getPieceList().get(0).getPosX());
        System.out.println(posNew.getPieceList().get(0).getPosY());
        
        System.out.println("clonozott objektum adattagjának megváltozatás után");
        System.out.println("a prototípus értékének kiiratása");
        System.out.print(pos.getPieceList().get(0).getPosX());
        System.out.println(pos.getPieceList().get(0).getPosY());
     
        
    }
    
}
