
package javaapplication8;

import java.io.Serializable;


public class ChessPiece implements Serializable{
    
    private char posX;
    private int posY;

    public ChessPiece(char posX, int posY) {
        this.posX = posX;
        this.posY = posY;
    }

    public ChessPiece() {
    }
    
    

    public char getPosX() {
        return posX;
    }

    public void setPosX(char posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }
    
    
    
}
