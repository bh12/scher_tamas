package javaapplication8;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class StartingPos implements Cloneable {

    private List<ChessPiece> pieceList;

    public StartingPos() {
        pieceList = new ArrayList<>();
        initStartPos();
        System.out.println("init elindul igy hogy a konstroktorban hivjuk meg");
    }

    public StartingPos(List<ChessPiece> list) {
        this.pieceList = list;
    }

    public List<ChessPiece> getPieceList() {
        return pieceList;
    }

    public void initStartPos() {
        ChessPiece piece = new ChessPiece('a', 8);
        pieceList.add(piece);
    }

    // nem jó így a clonozás mert az prototípus listában szereplő objektumok
    //nem deep copyk lesznek csak shalow copyk, a cikkben a deep copyt csak a listára értette, viszont
    //az a lista stringeket tartalmazott ami immutable osztály, ezért annak mindegy
    //hogy shalow copy vagy deep copy mert abból uj jön létre igy is ugyis
    //viszont ha a prototipus lista nem immutable referencia típusokat tartalmazz 
    //akkor ez a clone metodus nem jó mert a clonozott listában lévő objektumok
    //referenciái is az eredeti prototipus listában lévő objektumokra mutatnak
    @Override
    protected Object clone() throws CloneNotSupportedException {
        List<ChessPiece> temp = new ArrayList<>();
        for (ChessPiece ge : this.getPieceList()) {
            temp.add(ge);
        }
        return new StartingPos(temp);
    }
    
    //a következő megoldás deep copyt csinál a listában lévő objektumokról is
    //a for each ciklusban példányosítani kell uj objektumot és az értékeit aztán besetteljük
    //ugyanazokkal az értékekkel mint amik a prototípus listában lévő objektumoknak van
    //ez egy megoldás deep copyra de minél több az adattag és ha azok is entityk akkor azokat is külön ujra példányosítani kell
    //szóval ez elég bonyolultnak tünik nekem
//    @Override
//    protected Object clone() throws CloneNotSupportedException {
//        List<ChessPiece> temp = new ArrayList<>();
//        for (ChessPiece ge : this.getPieceList()) {
//            ChessPiece newPiece = new ChessPiece();
//            newPiece.setPosX(ge.getPosX());
//            newPiece.setPosY(ge.getPosY());
//            temp.add(newPiece);
//        }
//        return new StartingPos(temp);
//    }
    
    
    //deep copyra serializásiós megoldás látható lejjebb, sztem ez a legjobb, a feljebb lévő deep copy nem tudom hogy regálna
    //arra h a mi GamePieceEntitynkben van még egy PieceEntity is amit uyanugy new kulcsszóval létre kell hozni
    
//    @Override
//    public Object clone() throws CloneNotSupportedException {
//        List<ChessPiece> temp = new ArrayList<>();
//        for (ChessPiece ge : this.getPieceList()) {
//            ChessPiece newPiece = deepCopy(ge);
//            temp.add(newPiece);
//        }
//        return new StartingPos(temp);
//    }
//
//    public static ChessPiece deepCopy(ChessPiece piece) {
//        try {
//            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
//            objectOutputStream.writeObject(piece);
//            ByteArrayInputStream bais = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
//            ObjectInputStream objectInputStream = new ObjectInputStream(bais);
//            return (ChessPiece) objectInputStream.readObject();
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
    
    
}


