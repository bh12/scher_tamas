package threadfilewriter;

import java.util.LinkedList;
import java.util.Scanner;

public class StringRequestor implements Runnable {

    LinkedList<String> lineList;

    public StringRequestor(LinkedList<String> lineList) {
        this.lineList = lineList;
    }

    @Override
    public void run() {
        boolean isInterrupted = false;
        while (!isInterrupted) {
            //System.out.println(Thread.currentThread().isInterrupted()); ezt nem tudtam true-vá változtatni ezért csináltam sajátot
            System.out.println("Adj meg eltárolandó mondatokat");
            Scanner sc = new Scanner(System.in);

            String line = sc.nextLine();

            while (!line.isEmpty()) {

                synchronized (lineList) {
                    lineList.add(line);
                }

                line = sc.nextLine();

                if (line.isEmpty()) {
                    isInterrupted = true;

                }
                System.out.println("isInterupetd: " + isInterrupted);
            }
        }

    }

}
