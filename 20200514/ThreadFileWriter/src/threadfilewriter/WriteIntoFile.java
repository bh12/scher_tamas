package threadfilewriter;

import java.io.File;
import java.util.LinkedList;

public class WriteIntoFile implements Runnable {

    LinkedList<String> lineList;

    public WriteIntoFile(LinkedList<String> lineList) {
        this.lineList = lineList;
    }

    @Override
    public void run() {
        File file = new File("result.txt");

        String line;

        try (java.io.FileWriter fw = new java.io.FileWriter(file)) {
            //BufferedWriter bw = new BufferedWriter(fw);
            while (!lineList.isEmpty()) {
                synchronized (lineList) {
                    line = lineList.poll();
                }
                
                fw.write(line + "\n");
                
            }

        } catch (Exception e) {
            System.out.println("File hiba");
        }
    }

}
