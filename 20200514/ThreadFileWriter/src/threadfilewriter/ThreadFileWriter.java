package threadfilewriter;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

public class ThreadFileWriter {

    public static LinkedList<String> lineList = new LinkedList<>();

    public static void main(String[] args) throws InterruptedException {
        // TODO code application logic here

        Thread stringRequestThread = new Thread(new StringRequestor(lineList));
        Thread fileWriterThread = new Thread(new WriteIntoFile(lineList));
        
        stringRequestThread.start();
        fileWriterThread.start();

        //a lentit még kiszervezni külön osztályba és külön szállra ,
        //a tryon belül a while ciklust még végessé alakítani
        /*
        File file = new File("result.txt");
        
        String line;
       
        
        try (WriteIntoFile fw = new WriteIntoFile(file)) {
            //BufferedWriter bw = new BufferedWriter(fw);
            while (true) {
            line = lineList.poll();
                fw.write(line + "\n");
            }
                
        }
        catch (Exception e) {
            System.out.println("File hiba");
        }
         */
    }

}
