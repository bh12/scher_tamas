
package threadfilewriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class proba {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        System.out.println(line);
        
        if (line.isEmpty()) {
            System.out.println("ezt megcsinálja");
        }
        
        File proba = new File("proba.txt");
        
       
        try (FileWriter fw = new FileWriter(proba)) {
            fw.write(line);
           
        } catch (IOException ex) {
            Logger.getLogger(proba.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
