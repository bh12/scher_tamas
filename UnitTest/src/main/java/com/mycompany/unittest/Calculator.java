
package com.mycompany.unittest;


public class Calculator {
    
    public long add (long a, long b) {
        return a + b;
    }
    
    public long multiply (long a, long b) {
        return a * b;
    }
    
    public double divide (double a, double b) {
        return a / b;
    }
    
}
