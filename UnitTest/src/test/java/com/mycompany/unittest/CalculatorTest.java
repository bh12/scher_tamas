/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.unittest;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author scher
 */
public class CalculatorTest {
    
    Calculator calc;
    
    public CalculatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        calc = new Calculator();
                
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
    public void add_sumOf3and5_returns8() {
    long result = calc.add(3l,5l);
        assertEquals(8l, result);
    }
    
    @Test
    public void multiply_multiplyOf2and9_retunrs18() {
        long result = calc.multiply(2l, 9l);
        assertEquals(18l, result);
    }
    
  
}
