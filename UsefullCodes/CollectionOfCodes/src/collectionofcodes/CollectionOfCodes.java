/*
mainben metudos nevére rámutatok, ctrl és balgomb - odaugrik a keresett metodusra
 */
package collectionofcodes;

public class CollectionOfCodes {

    public static int[] array = new int[5];
    public static int[][] array2D = new int[3][4];

    public static void main(String[] args) {

        int[] exampleArray = {2, 9, 5, 3, 4, 2};
        //System.out.println(linearSearch(exampleArray, 7));

        int[] sortedArray = {3, 5, 7, 9, 11};
        //System.out.println(binarySearch(sortedArray, 11));

        //sortArrayAsc(exampleArray);
        //sortArrayDesc(exampleArray);

        //fill1DArray(5, 25);
        //print1DArray(array);
        //fill2DArray(5, 8);
        //print2DArray();
        //System.out.println("");
        //System.out.println(minElement());
        //System.out.println(maxElement());

        //calculateMatrixAddition(array2D, array2D);
        //calculateMatrixProduct(array2D, array2D);
        //uj mátrixba ugy teszem az eredményt hogy int[][] newMatrix = calculateMatrixAddition(array2D, array2D);
        //double avg = avgArray(exampleArray);
        //System.out.println(avg);
        
        // kiválasztás megmutatja hányadik helyen van a keresett szám
        //System.out.println(kivalasztas(exampleArray, 2));

        //String split
        /*String string = "Lajos,Béla,Cukor,Só,Minden,Mi,Jó";
        System.out.println(string);

        String[] stringArray = string.split(",");
        for (String s : stringArray) {
            System.out.println(s);
        }
         */
    } // main vége

    public static void print1DArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + ", ");

        }
    }

    public static void print2DArray() {
        for (int i = 0; i < array2D.length; i++) {
            for (int j = 0; j < array2D[i].length; j++) {
                System.out.print(array2D[i][j] + ", ");

            }
            System.out.println("");

        }
    }

    public static int randomNumber(int min, int max) {
        return (int) (Math.random() * (max - min + 1) + min);
    }

    public static void fill1DArray(int min, int max) {
        for (int i = 0; i < array.length; i++) {
            array[i] = randomNumber(min, max);

        }
    }

    public static void fill2DArray(int min, int max) {
        for (int i = 0; i < array2D.length; i++) {
            for (int j = 0; j < array2D[i].length; j++) {

                array2D[i][j] = randomNumber(min, max);
            }
        }
    }

    //lineáris keresés minden tömbön müködik nemcsak rendezetten
    public static boolean linearSearch(int[] array, int number) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                return true;
            }
        }

        return false;
    }

    /*
        bináris keresés
        csak rendezett tömbön működik
        log(n) keresés
     */
    //3 5 7 9 11 keressuk a 11-et
    public static boolean binarySearch(int[] sortedArray, int number) {
        int startIndex = 0;
        int endIndex = sortedArray.length;
        int middleIndex = (startIndex + endIndex) / 2;

        while (sortedArray[middleIndex] != number && startIndex < middleIndex) {
            if (sortedArray[middleIndex] < number) {
                startIndex = middleIndex + 1;
            } else {
                endIndex = middleIndex - 1;
            }

            middleIndex = (startIndex + endIndex) / 2;
        }

        //return sortedArray[middleIndex] == number;
        if (sortedArray[middleIndex] == number) {
            return true;
        } else {
            return false;
        }
    }

    // buborék elnredezés növekvő sorrendbe
    public static void sortArrayAsc(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = array.length - 1; j > i; j--) {
                if (array[i] > array[j]) {//csere
                    int tmp = array[i];
                    array[i] = array[j];
                    array[j] = tmp;
                }
            }
        }
    }

    // buborék elnredezés csökkenő sorrendbe
    public static void sortArrayDesc(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = array.length - 1; j > i; j--) {
                if (array[i] < array[j]) {
                    int tmp = array[i];
                    array[i] = array[j];
                    array[j] = tmp;
                }
            }
        }
    }

    public static int[][] calculateMatrixAddition(int[][] matrixA, int[][] matrixB) {
        int[][] tempResult = new int[matrixA.length][matrixA[0].length];
        for (int i = 0; i < matrixA.length; i++) {
            for (int j = 0; j < matrixA[i].length; j++) {
                tempResult[i][j] = matrixA[i][j] + matrixB[i][j];
            }
        }

        return tempResult;
    }

    public static int[][] calculateMatrixProduct(int[][] matrixA, int[][] matrixB) {
        int[][] tempResult = new int[matrixA.length][matrixA[0].length];
        for (int i = 0; i < tempResult.length; i++) {
            for (int j = 0; j < tempResult[i].length; j++) {
                for (int z = 0; z < matrixA.length; z++) {
                    tempResult[i][j] += matrixA[i][z] * matrixB[z][j];
                }
            }
        }

        return tempResult;
    }

    // átlagot add, ha csak az összeg kell akkor return sum oszt kész
    public static double avgArray(int[] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        return (double) sum / array.length;
    }

    // megadja hogy hányadik helyen van a keresett szám
    public static int kivalasztas(int[] array, int searchedNumber) {
        int i = 0;
        while (array[i] != searchedNumber) {
            i++;
        }

        return i;
    }
    
    public static int maxElement() {
        int max = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
            
        }
        return max;
    }
    
    public static int minElement() {
        int min = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
            }
            
        }
        return min;
    }

}
