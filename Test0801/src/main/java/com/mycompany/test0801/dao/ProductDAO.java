
package com.mycompany.test0801.dao;

import com.mycompany.test0801.entity.ProductEntity;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Singleton
@TransactionAttribute
public class ProductDAO {
    
    @PersistenceContext
    private EntityManager em;
    
    public List<ProductEntity> listProducts() {
        return em.createNamedQuery(ProductEntity.QUERY_SELECT_ALL, ProductEntity.class).getResultList();
        
    }
    
}
