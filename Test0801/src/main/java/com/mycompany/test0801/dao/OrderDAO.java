package com.mycompany.test0801.dao;

import com.mycompany.test0801.dto.OrderDTO;
import com.mycompany.test0801.entity.OrderEntity;
import com.mycompany.test0801.mapper.OrderMapper;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Singleton
public class OrderDAO {

    @PersistenceContext
    EntityManager em;

    public List<OrderDTO> getAllOrders() {

        List<OrderEntity> orders = em.createQuery("select o from OrderEntity o").getResultList();
        //itt átmeppelni dto-ra 

        List<OrderDTO> dtos = new ArrayList<>();
        for (OrderEntity order : orders) {
            dtos.add(OrderMapper.toDTO(order));

        }
        return dtos;
    }
}
