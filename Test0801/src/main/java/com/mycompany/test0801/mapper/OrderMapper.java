package com.mycompany.test0801.mapper;

import com.mycompany.test0801.dto.OrderDTO;
import com.mycompany.test0801.entity.OrderEntity;
import java.util.ArrayList;
import java.util.List;

public class OrderMapper {

    public static OrderDTO toDTO(OrderEntity oe) {

        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setId(oe.getId());
        orderDTO.setDeliveryDate(oe.getDeliveryDate());
        orderDTO.setOrderDate(oe.getOrderDate());

        return orderDTO;
    }

    public static List<OrderDTO> toDTOList(List<OrderEntity> orders) {
        List<OrderDTO> dtoList = new ArrayList<>();
        for (OrderEntity order : orders) {
            dtoList.add(toDTO(order));
        }
        return dtoList;
    }

}
