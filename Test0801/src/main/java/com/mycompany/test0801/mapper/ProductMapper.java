
package com.mycompany.test0801.mapper;

import com.mycompany.test0801.dto.ProductDTO;
import com.mycompany.test0801.entity.ProductEntity;


public class ProductMapper {
    
    public static ProductDTO toDTO(ProductEntity productEntity) {
        
        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(productEntity.getId());
        productDTO.setDescription(productEntity.getDescription());
        productDTO.setPrice(productEntity.getPrice());
        productDTO.setOrderEntity(productEntity.getOrder());
        
        
        return productDTO;
        
    }
    
}
