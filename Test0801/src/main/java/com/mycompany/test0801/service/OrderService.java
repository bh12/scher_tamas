
package com.mycompany.test0801.service;

import com.mycompany.test0801.dao.OrderDAO;
import com.mycompany.test0801.dto.OrderDTO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class OrderService {
    
    @EJB
    OrderDAO orderDAO;
    
    public List<OrderDTO> getAllOrders() {
        return orderDAO.getAllOrders();
    }
    
}
