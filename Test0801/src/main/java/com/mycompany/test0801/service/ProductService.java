
package com.mycompany.test0801.service;

import com.mycompany.test0801.dao.ProductDAO;
import com.mycompany.test0801.entity.OrderEntity;
import com.mycompany.test0801.entity.ProductEntity;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.inject.Inject;

@Singleton
@TransactionAttribute
public class ProductService {
    
    @Inject
    private ProductDAO productDAO;
    
    public List<ProductEntity> listProducts() {
        return productDAO.listProducts();
    }
    
}
