
package com.mycompany.test0801.servlet;

import com.mycompany.test0801.dto.OrderDTO;
import com.mycompany.test0801.service.OrderService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "OrderServlet", urlPatterns = {"/OrderServlet"})
public class OrderServlet extends HttpServlet {

  @Inject
  OrderService os;
  

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       List<OrderDTO> orders = os.getAllOrders();
       
       req.setAttribute("orders", orders);
       
       req.getRequestDispatcher("WEB-INF/orders.jsp").forward(req, resp);
    }
  
  

}
