<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>JSP Page</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
              integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
    <body>
        <h1>This is the list of orders</h1>
     

        <table class="table">
            <thead>
                <tr>
                  
                    <th scope="col">id</th>
                    <th scope="col">deliveryDate</th>
                    <th scope="col">orderDate</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${orders}" var="o">
                    <tr>
                      
                        <td>${o.id}</td>
                        <td>${o.deliveryDate}</td>
                        <td>${o.orderDate}</td>
                        <c:forEach itmes="${}" var="">
                            <td></td>>
                                
                            
                        </c:forEach>
                    </tr>
                </c:forEach>
            </tbody>
        </table>

    </body>
</html>
