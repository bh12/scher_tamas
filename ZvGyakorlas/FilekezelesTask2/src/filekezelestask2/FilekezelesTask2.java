
package filekezelestask2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class FilekezelesTask2 {


    public static void main(String[] args) {
        // TODO code application logic here
        File origin = new File("origin.txt");
        File target = new File("target.txt");
        List<String> lines = readFromFile(origin);
        printList(lines);

        writeFile(lines);
        
    }
    
    public static List<String> readFromFile(File f) {
        
        List<String> stringList = new ArrayList<>();
        try(BufferedReader br = new BufferedReader(new FileReader("origin.txt"))) {
            
            String line = br.readLine();
            
            while(line != null)
            {
                stringList.add(line);
                line = br.readLine();
                
            }
            
                
             
        } catch (IOException ex) {
            System.out.println("File hiba");
        }
        
        return stringList;
        
    }
    
     public static void printList(List<String> list) {
        for (String line : list) {
            System.out.println(line);
        }
    }
     
     public static void writeFile(List<String> lines) {
         
         try (BufferedWriter bw = new BufferedWriter(new FileWriter("target.txt"))) {
             
             for (int i = 1; i < lines.size(); i += 2) {
                 bw.write(lines.get(i));
                bw.newLine();
             }
             
            
         } catch (IOException ex) {
             System.out.println("File hiba");
         }
     }

    
}
