
package com.mycompany.task4addnumbers.servlet;

import com.mycompany.task4addnumbers.exeption.NotIntegerExeption;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "AddNumbers", urlPatterns = {"/AddNumbers"})
public class AddNumbers extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       
        try {
        String number1 = req.getParameter("number1");
        String number2 = req.getParameter("number2");
        
            addNumbers(number1, number2);
        
        } catch (NotIntegerExeption e) {
            System.out.println("nem szám");
        } 
        
        
    }
    
    public static void addNumbers(String number1, String number2) throws NotIntegerExeption {
        
        String regex = "\\d+";
       
        
        if (!number1.matches(regex) || !number2.matches(regex)) {
            throw new NotIntegerExeption();
        }
        
        else {
            String result = String.valueOf(Integer.valueOf(number1) + Integer.valueOf(number2));
            System.out.println(result);
        }
        
        
       
    }

   

}
