
package task6csvhandling;


public class StudentResults {
    
    private String name;
    private int zh1;
    private int zh2;
    private int zv;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getZh1() {
        return zh1;
    }

    public void setZh1(int zh1) {
        this.zh1 = zh1;
    }

    public int getZh2() {
        return zh2;
    }

    public void setZh2(int zh2) {
        this.zh2 = zh2;
    }

    public int getZv() {
        return zv;
    }

    public void setZv(int zv) {
        this.zv = zv;
    }
    
    
    
}
