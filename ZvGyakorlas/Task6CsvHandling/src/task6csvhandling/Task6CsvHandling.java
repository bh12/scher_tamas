package task6csvhandling;

import static java.awt.PageAttributes.MediaType.A;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;
import java.util.stream.Stream;

public class Task6CsvHandling {

    public static List<StudentResults> results = new ArrayList<>();
    

    public static void main(String[] args) {
        // TODO code application logic here
        csvReader();
        csvWriter();
    }

    public static void csvReader() {

        try (BufferedReader br = new BufferedReader(new FileReader("detail.csv"))) {

            String line;

            while ((line = br.readLine()) != null) {

                String[] values = line.split(";");
                StudentResults student = new StudentResults();
                student.setName(values[0]);
                student.setZh1(Integer.valueOf(values[1]));
                student.setZh2(Integer.valueOf(values[2]));
                student.setZv(Integer.valueOf(values[3]));

                results.add(student);
            }

        } catch (IOException ex) {
            System.out.println("File hiba");

        }

    }

    public static List<String> avgCalc() {
        
        List<String> data = results.stream().map((StudentResults s) -> {double avg = (s.getZh1() + s.getZh2() + s.getZv())/3.0;
                                                    String name = s.getName();
                                                    String avgWithName = name + ";" + avg;
                                                    return avgWithName;
        
        }).collect(Collectors.toList());
              
        return data;
    }
    
        
    public static void csvWriter() {
        
        try (BufferedWriter bw = new BufferedWriter(new FileWriter("avg.csv"))) {
            
            List<String> data = avgCalc();
            for (String line : data) {
                bw.write(line);
                bw.newLine();
                bw.flush();
                
            }
            
            
        } catch (IOException exp) {
            System.out.println("kiiráskori file hiba");
        }
        
    }

}
