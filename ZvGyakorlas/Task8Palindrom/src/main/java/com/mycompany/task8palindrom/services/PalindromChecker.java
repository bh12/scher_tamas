
package com.mycompany.task8palindrom.services;


public class PalindromChecker {
    
    public static boolean isPalindrom(String text) {
        
        int i = 0;
        int j = text.length() - 1;
        
        while (i < j) {
            if (text.charAt(i) != text.charAt(j)) {
                    return false;
                }
            
            i++;
            j--;
        }
        
        
        return true;
    }
    
}
