<%-- 
    Document   : carStorage
    Created on : Sep 23, 2020, 1:44:19 PM
    Author     : scher
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>carStorage</title>
    </head>
    <body>
        <form method="POST" action="SaveCarServlet" onsubmit="return validateForm()">
            
                <div class="form-group">
                    <label for="plateNumber">plateNumber:</label>
                    <input type="text" class="form-control" placeholder="Enter plateNumber" name="plateNumber" id="plateNumber">
                </div>
            
                <div class="form-group">
                    <label for="type">type:</label>
                    <input type="text" class="form-control" placeholder="Enter type" name="type" id="type">
                </div>
            
            <div class="form-group">
                    <label for="km">km:</label>
                    <input type="text" class="form-control" placeholder="Enter km" name="km" id="km">
                </div>
            
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
    </body>
</html>
