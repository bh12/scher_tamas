
package com.mycompany.task5cardata.servlet;

import com.mycompany.task5cardata.dto.CarDTO;
import com.mycompany.task5cardata.service.CarService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "SaveCarServlet", urlPatterns = {"/SaveCarServlet"})
public class SaveCarServlet extends HttpServlet {
    
    @Inject
    CarService carService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("WEB-INF/carStorage.jsp").forward(req, resp);
    }
    
    

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       
        String plateNumber = req.getParameter("plateNumber");
        String type = req.getParameter("type");
        int km = Integer.valueOf(req.getParameter("km"));
        
        CarDTO carDTO = new CarDTO();
        carDTO.setPlateNumber(plateNumber);
        carDTO.setType(type);
        carDTO.setKm(km);
        
        carService.saveCar(carDTO);
        
        System.out.println(plateNumber + type + km);
    }


}
