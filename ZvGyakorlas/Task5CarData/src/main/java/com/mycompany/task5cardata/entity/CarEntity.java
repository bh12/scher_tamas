
package com.mycompany.task5cardata.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "car")
public class CarEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column
    private String plateNumber;
    
    @Column
    private String type;
    
    @Column
    private int km;
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

       
}
