package com.mycompany.task5cardata.dao;

import com.mycompany.task5cardata.dto.CarDTO;
import com.mycompany.task5cardata.entity.CarEntity;
import com.mycompany.task5cardata.mapper.CarMapperInterface;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Singleton
public class CarDAO {
    
    private static final String QUERY_MIN_KM = "SELECT MIN(c.km) FROM CarEntity c";
    
    @PersistenceContext
    private EntityManager em;
    
    public void saveCar(CarDTO carDTO) {
        
        CarEntity carEntity = CarMapperInterface.INSTANCE.toEntity(carDTO);
        
        em.persist(carEntity);
        
    }
    
    public void selectMinKm() {
        
        Query minKm = em.createQuery(QUERY_MIN_KM);
        
        int carEntity = (Integer)minKm.getSingleResult();
        
        System.out.println(carEntity);
        
    }
    
}
