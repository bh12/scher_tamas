
package com.mycompany.task5cardata.dto;

import lombok.Data;

@Data
public class CarDTO {
    
     private Long id;
    
     private String plateNumber;
    
    private String type;
    
    private int km;
    
}
