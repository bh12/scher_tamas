
package com.mycompany.task5cardata.service;

import com.mycompany.task5cardata.dao.CarDAO;
import com.mycompany.task5cardata.dto.CarDTO;
import com.mycompany.task5cardata.entity.CarEntity;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.inject.Inject;


@Singleton
@TransactionAttribute
public class CarService {
    
    @Inject
    CarDAO carDAO;
    
    public void saveCar(CarDTO carDTO) {
        
        carDAO.saveCar(carDTO);
    }
    
    public void selectMinKm() {
        carDAO.selectMinKm();
    }
    
}
