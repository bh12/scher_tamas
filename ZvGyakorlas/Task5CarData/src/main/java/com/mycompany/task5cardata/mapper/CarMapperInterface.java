
package com.mycompany.task5cardata.mapper;

import com.mycompany.task5cardata.dto.CarDTO;
import com.mycompany.task5cardata.entity.CarEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CarMapperInterface {
    
    CarMapperInterface INSTANCE = Mappers.getMapper(CarMapperInterface.class);
    
    CarEntity toEntity(CarDTO carDTO);
    
}
