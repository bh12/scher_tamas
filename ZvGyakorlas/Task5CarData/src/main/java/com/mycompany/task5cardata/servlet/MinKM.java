
package com.mycompany.task5cardata.servlet;

import com.mycompany.task5cardata.service.CarService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "MinKm", urlPatterns = {"/MinKm"})
public class MinKM extends HttpServlet {
    
    @Inject
    CarService carService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        carService.selectMinKm();
        
        req.getRequestDispatcher("WEB-INF/minKm.jsp").forward(req, resp);
    }



}
