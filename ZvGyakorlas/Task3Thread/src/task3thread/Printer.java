
package task3thread;

import java.util.concurrent.atomic.AtomicInteger;


public class Printer implements Runnable{
    
   private static AtomicInteger counter = new AtomicInteger(0);

   
    @Override
    public void run() {
        int rnd = 100;
       for (int i = 0; i < rnd; i++) {
        
        System.out.println(Thread.currentThread().getName() + ": " + counter.getAndIncrement());
            
        }
    }
   
    
    
}
