
package hw20200409fruits;

public class Fruits {
    
    protected String color;
    protected double pricePerKg;

   

    public String getColor() {
        return color;
    }

    public double getPricePerKg() {
        return pricePerKg;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setPricePerKg(int pricePerKg) {
        this.pricePerKg = pricePerKg;
    }

    @Override
    public String toString() {
        return "Fruits{" + "color=" + color + ", pricePerKg=" + pricePerKg + '}';
    }
    
    
    
}
