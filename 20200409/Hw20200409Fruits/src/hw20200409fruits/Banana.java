
package hw20200409fruits;

public class Banana extends Fruits {

    public Banana(String color, int pricePerKg) {
        this.color= color;
        this.pricePerKg = pricePerKg;
    }

    @Override
    public double getPricePerKg() {
        return super.getPricePerKg() / 1000; 
    }

    @Override
    public String toString() {
        return "Banana{" + "color=" + getColor() + ", pricePerDkg=" + getPricePerKg() + '}';
    }

    
    
    
    
    
}
