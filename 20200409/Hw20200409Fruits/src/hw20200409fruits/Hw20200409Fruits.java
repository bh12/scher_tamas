/*
Hazi feladat: Legyen egy Fruit osztalyod. Minden Fruitnak van valamilyen szine es kilogrammonkenti ara.
Ebbol szarmaztass le egy Banana, Apple, Orange osztalyt. A Banana osztaly specialis, 
mert annak mindig a dekagrammonkenti arat szeretnenk megtudni. 
Az Applenek van iz tipusa is. Pl lehet fanyar, edes, stb.. egy Apple. 
Az Orange mindig naranccsarga szinu.
*/

package hw20200409fruits;

public class Hw20200409Fruits {

    public static void main(String[] args) {
        // TODO code application logic here
        Banana banana1 = new Banana("yellow", 500);
        System.out.println(banana1);
        
        Fruits banana2 = new Banana("green", 400);
        System.out.println(banana2);
        
        Apple apple1 = new Apple("sweet", "red", 430);
        System.out.println(apple1);
        
        Orange orange1 = new Orange(600);
        System.out.println(orange1);
        
        banana2.setColor("lila");
        System.out.println(banana2);
                
        
    }
    
}
