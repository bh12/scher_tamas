
package hw20200409fruits;

public class Apple extends Fruits {
    
    private String taste;

    public Apple(String taste, String color, double pricePerKg) {
        this.color = color;
        this.pricePerKg = pricePerKg;
        this.taste = taste;
    }

    public String getTaste() {
        return taste;
    }

    public void setTaste(String taste) {
        this.taste = taste;
    }

    @Override
    public String toString() {
        return "Apple{" + "taste=" + taste + ", color=" + getColor() + ", pricePerKg=" + getPricePerKg() + '}';
    }
    
    
    
}
