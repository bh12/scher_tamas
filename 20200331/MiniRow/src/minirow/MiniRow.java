package minirow;

/*2. generáljunk egy tömböt véletlen számokkal, majd írjuk ki azt a számot, 
amikortól kezdve tőle csak nagyobb szám van a tömbben. 
pl. 
3, 4, 7, 3, 4, 6 esetén a 3 a megoldás
9, 3, 1, 5, 2, 4, 6 esetén a 2 a megoldás //Ebből az derül ki hogy utána növekvő sornak kell
lennie és nem csak annyi h adott számtol minegyik nagyobb legyen de nem szükséges növekvő sorrend, 
mert itt akkor 1-lenne a megoldás
 */
public class MiniRow {

    public static void main(String[] args) {
        // TODO code application logic here

        //int[] array = {3, 4, 7, 3, 1, 4, 2, 6};
        int[] array = new int[15];

        fillArray(array, 20, 0);

        int searchedIndex = array.length - 1;
        int i = array.length - 1;

        do {
            if ((array[i] - array[i - 1]) > 0) {
                searchedIndex = i - 1;
                i--;
            }
        } while ((array[i] - array[i - 1]) > 0 && i >= 1);

       

        printArray(array);

        System.out.println(array[searchedIndex]);
    }

    public static int randomNumber(int min, int max) {
        return (int) (Math.random() * (max - min + 1) + min);
    }

    public static void fillArray(int[] array, int min, int max) {
        for (int i = 0; i < array.length; i++) {
            array[i] = randomNumber(min, max);

        }
    }

    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + ", ");

        }
        System.out.println("");
    }

}
