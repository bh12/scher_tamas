package bossworkerswing;

import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class View extends JFrame {

    private Controller controller;

    private JTextArea logArea;
    private JPanel panel;
    private JButton bossButton;
    private JButton workerButton;
    private JButton serialiseButton;
    private JButton deserialiseButton;

    public View(Controller controller) {
        this.controller = controller;
    }

    public void init() {
        setUpWindow();
        setUpButtons();

        showWindow();

    }

    private void showWindow() {
        this.setVisible(true);
    }

    private void setUpWindow() {
        this.setSize(500, 500);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setTitle("Boss Wroker handling");
        this.setLayout(new GridLayout(2, 1));

        logArea = new JTextArea();
        add(logArea);

        panel = new JPanel();
        add(panel);

    }

    private void setUpButtons() {

        bossButton = new JButton("create boss");
        bossButton.addActionListener(event -> this.controller.handleBossButton());
        panel.add(bossButton);

        workerButton = new JButton("create worker");
        workerButton.addActionListener(event -> this.controller.handleWorkerButton());
        panel.add(workerButton);

        serialiseButton = new JButton("serialise");
        serialiseButton.addActionListener(event -> this.controller.handelSerialization());
        panel.add(serialiseButton);

        deserialiseButton = new JButton("deserialise");
        deserialiseButton.addActionListener(event -> this.controller.handelDeserialization());
        panel.add(deserialiseButton);

    }

    public void setLogArea(String text) {
        this.logArea.append(text);
    }

}
