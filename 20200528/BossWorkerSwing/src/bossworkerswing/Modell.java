package bossworkerswing;

import java.util.ArrayList;

public class Modell {

    private ArrayList<Employee> employeeList;

    public Modell() {
        this.employeeList = new ArrayList<>();
    }

    public ArrayList<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(ArrayList<Employee> employeeList) {
        this.employeeList = employeeList;
    }

}
