package bossworkerswing;

import java.awt.List;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.*;
import java.util.ArrayList;

public class Controller {

    private View view;
    private Modell modell;

    public Controller() {
        this.view = new View(this);
        this.modell = new Modell();
        this.view.init();
    }

    public String dateTimeLogger() {
        LocalDateTime loggedTime = LocalDateTime.now();
        return loggedTime.toString();
    }

    public void handleBossButton() {

        Employee boss = new Boss();
        employeeToAddInArrayList(boss);

        String text = dateTimeLogger() + "; boss was created";

        this.view.setLogArea(text);

    }

    public void handleWorkerButton() {

        Employee worker = new Worker();
        employeeToAddInArrayList(worker);

        String text = dateTimeLogger() + "; worker was created";

        this.view.setLogArea(text);

    }

    public <T extends Employee> void employeeToAddInArrayList(T employee) {
        this.modell.getEmployeeList().add(employee);

    }

    public void handelSerialization() {
        File file = new File("employee.ser");

        try (FileOutputStream fos = new FileOutputStream(file);
                ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(this.modell.getEmployeeList());

        } catch (IOException ie) {
            System.out.println("File hiba");
            ie.getStackTrace();
        }

    }

    public void handelDeserialization() {
        File file = new File("employee.ser");

        ArrayList<Employee> newEmployeeList = new ArrayList<>();

        try (FileInputStream fis = new FileInputStream(file);
                ObjectInputStream ois = new ObjectInputStream(fis)) {
            newEmployeeList = (ArrayList) ois.readObject();

        } catch (IOException ie) {
            System.out.println("File hiba");
            ie.getStackTrace();
        } catch (ClassNotFoundException ce) {
            System.out.println("Class hiba");
            ce.getStackTrace();
        }

        for (Employee e : newEmployeeList) {
            System.out.println(e);
        }

    }

}
