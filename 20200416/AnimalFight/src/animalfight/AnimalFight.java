package animalfight;

import java.util.Random;

public class AnimalFight {

    public static final Random rnd = new Random();
    public static Creature[] creatures = new Creature[100];

    public static void main(String[] args) {

        //Creature[] creatures = new Creature[100];
        generateCreatures(creatures);

        int counter = 0;
        while (true) {
            //elso elo farkas
            Wolf attackerWolf = attacker();

            //elso elo nem farkas
            Animal poorAnimal = victim();

            if (attackerWolf == null || poorAnimal == null) {
                System.out.println("counter: " + counter);
                break;
            }

            counter++;

            System.out.println(poorAnimal);
            attackerWolf.attack();
            //elso harapas
            poorAnimal.setLiveScore(poorAnimal.getLiveScore() - attackerWolf.getAttackScore());
            if (poorAnimal instanceof AttackInterface) {//megnezem, hogy az adott allat implementalja-e az attack interfacet
                AttackInterface ai = (AttackInterface) poorAnimal;
                boolean bothAlive = true;
                while (bothAlive) {
                    ai.attack();
                    attackerWolf.setLiveScore(attackerWolf.getLiveScore() - ai.getAttackScore());

                    if (attackerWolf.isLive()) {
                        attackerWolf.attack();
                        poorAnimal.setLiveScore(poorAnimal.getLiveScore() - attackerWolf.getAttackScore());
                    }
                    bothAlive = attackerWolf.isLive() && poorAnimal.isLive();
                }

                Dog.underAttack = false;

            }
        } // while vége
    } // itt a main vége

    public static Animal victim() {
        Animal poorAnimal = null;
        for (int i = 0; i < creatures.length; i++) {
            if (!(creatures[i] instanceof Wolf) && creatures[i].isLive()) {
                poorAnimal = (Animal) creatures[i];
                break;
            }
        }
        return poorAnimal;
    }

    public static Wolf attacker() {
        Wolf attackerWolf = null;
        for (int i = 0; i < creatures.length; i++) {
            if (creatures[i] instanceof Wolf && creatures[i].isLive()) {
                attackerWolf = (Wolf) creatures[i];
                break;
            }
        }
        return attackerWolf;
    }

    public static void generateCreatures(Creature[] creatures) {
        for (int i = 0; i < creatures.length; i++) {
            int rndNumber = rnd.nextInt(100);
            if (rndNumber < 60) {
                creatures[i] = new Chicken(rnd.nextInt(5));
            } else if (rndNumber < 65) {
                creatures[i] = new Turkey(rnd.nextInt(5));
            } else if (rndNumber < 70) {
                creatures[i] = new Cat(rnd.nextInt(10));
            } else if (rndNumber < 80) {
                creatures[i] = new Dog(rnd.nextInt(20));
            } else {
                creatures[i] = new Wolf(rnd.nextInt(20));
            }
        }

    }
}

/*
Legyen egy kert, ahol vannak állatok, 100 db, csirke, macska stb.
Legyenek vadállatok, pl. farkas
minden állanak legyen hp-je, vadállatoknak damage

vadállatok betörnek a kertbe, próbálnak legyőzni háziállatokat
 */
