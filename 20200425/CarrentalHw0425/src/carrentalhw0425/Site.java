package carrentalhw0425;

import java.util.ArrayList;
import java.util.List;

public class Site {

    private int siteId;

    private String address;

    private List<Car> cars; 

    public Site(int siteId, List<Car> cars) {
        this.siteId = siteId;
        this.cars = cars;
    }
    
    

    public int getSiteId() {
        return siteId;
    }

    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    @Override
    public String toString() {
        return "siteId=" + siteId;
    }
    
    

}
