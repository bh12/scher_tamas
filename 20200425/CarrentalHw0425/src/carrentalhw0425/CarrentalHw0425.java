package carrentalhw0425;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CarrentalHw0425 {

    public static Scanner sc = new Scanner(System.in);
    
    public static final int DEFAULT_CAR_NUMBER = 2;
    public static List<Car> carList2 = new ArrayList<>();
    
    

    public static void main(String[] args) {
        // TODO code application logic here
        DieselCar diesel1 = new DieselCar(40, "BMW", "520D", 100000);
        diesel1.maxDistanceCalculator();
    
        ElectricCar electric1 = new ElectricCar(50, "BMW", "I3", 40000);
        electric1.maxDistanceCalculator();
           
        PetrolCar petrol1 = new PetrolCar(40, "BMW", "M3", 100000);
        petrol1.maxDistanceCalculator();
      
        
        List<Car> carList1 = new ArrayList<>();
      
        Site site1 = new Site(1, carList1);
        
        List<Site> siteList1 = new ArrayList<>();
        
        System.out.println(site1);
        
        Company company1 = new Company(siteList1);
      
        carDataUpload();

    }
    
    public static void carDataUpload() {
        int carCounter = 0;
        String tempCarDataStorgae;
        do {
            System.out.println("Add meg a kocsi adatait veszővel elválasztva");
            tempCarDataStorgae = sc.next();
            System.out.println(tempCarDataStorgae);
            ++carCounter;
            
        } while (carCounter != DEFAULT_CAR_NUMBER);
        
    }
}
