package carrentalhw0425;

import java.util.ArrayList;
import java.util.List;

public class Company {

    private List<Site> sites; 

    public Company(List<Site> sites) {
        this.sites = sites;
    }
    
    

    public List<Site> getSites() {
        return sites;
    }

    public void setSites(List<Site> sites) {
        this.sites = sites;
    }

}
