
package carrentalhw0425;


public class ElectricCar extends Car implements DailyPrice{
    
    private double akkuChargeLevel; //max 1 az értéke ezt vhogy beállítani
    private double efficiencyRatio;
    private int maxDistance;

    public ElectricCar(int akkuChargeLevel, String carBrand, String model, int kilometerCount) {
        super(carBrand, model, kilometerCount);
        this.akkuChargeLevel = akkuChargeLevel / 100.0;
    }

    
    @Override
    public int maxDistanceCalculator() {
        setEfficiencyRatio();
        setMaxDistance((int) (this.akkuChargeLevel * 350 * this.getEfficiencyRatio()));
        return maxDistance;
    }

    public double getAkkuChargeLevel() {
        return akkuChargeLevel;
    }

    public double getEfficiencyRatio() {
        return efficiencyRatio;
    }

    public int getMaxDistance() {
        return maxDistance;
    }

    public void setAkkuChargeLevel(int akkuChargeLevel) {
        this.akkuChargeLevel = akkuChargeLevel / 100.0;
    }

    public double setEfficiencyRatio() {
        if ((1.0 - (super.getKilometerCount() / 280000.0)) > 0.6) {
            return this.efficiencyRatio = 1.0 - (super.getKilometerCount() / 280000.0);
        } else {
            return this.efficiencyRatio = 0.6;
        }
    }

    public void setMaxDistance(int maxDistance) {
        this.maxDistance = maxDistance;
    }

    @Override
    public String toString() {
        return "ElectricCar: " + super.toString() + "akkuChargeLevel=" + akkuChargeLevel + ", maxDistance=" + maxDistance;
    }
    
    
    
    
}
