package carrentalhw0425;

public abstract class Car {

    private String plateNumber;

    private String color;

    private String carBrand;

    private String model;

    private int doorCount;

    private int kilometerCount;

    private int yearOfCreation;

    private int wieght;

    public Car(String carBrand, String model, int kilometerCount) {
        this.carBrand = carBrand;
        this.model = model;
        this.kilometerCount = kilometerCount;
    }

    public abstract int maxDistanceCalculator();

    public String getPlateNumber() {
        return plateNumber;
    }

    public String getColor() {
        return color;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public String getModel() {
        return model;
    }

    public int getDoorCount() {
        return doorCount;
    }

    public int getKilometerCount() {
        return kilometerCount;
    }

    public int getYearOfCreation() {
        return yearOfCreation;
    }

    public int getWieght() {
        return wieght;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setDoorCount(int doorCount) {
        this.doorCount = doorCount;
    }

    public void setKilometerCount(int kilometerCount) {
        this.kilometerCount = kilometerCount;
    }

    public void setYearOfCreation(int yearOfCreation) {
        this.yearOfCreation = yearOfCreation;
    }

    public void setWieght(int wieght) {
        this.wieght = wieght;
    }

    @Override
    public String toString() {
        return "carBrand=" + carBrand + ", model=" + model + ", kilometerCount=" + kilometerCount + ", ";
    }
    
    

}
