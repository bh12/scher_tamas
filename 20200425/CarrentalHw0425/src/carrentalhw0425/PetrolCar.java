
package carrentalhw0425;


public class PetrolCar extends Car implements DailyPrice{
    
    private int fuelLevel;
    private double efficiencyRatio;
    private int maxDistance;

    public PetrolCar(int fuelLevel, String carBrand, String model, int kilometerCount) {
        super(carBrand, model, kilometerCount);
        this.fuelLevel = fuelLevel;
    }
    
    
    @Override
    public int maxDistanceCalculator() {
        setEfficiencyRatio();
        setMaxDistance((int) (this.fuelLevel * 10 * this.getEfficiencyRatio()));
        return maxDistance;
    }

    public int getMaxDistance() {
        return maxDistance;
    }

    public void setMaxDistance(int maxDistance) {
        this.maxDistance = maxDistance;
    }
    
     
    
    public int getFuelLevel() {
        return fuelLevel;
    }

    public double getEfficiencyRatio() {
        return efficiencyRatio;
    }

    public void setFuelLevel(int fuelLevel) {
        this.fuelLevel = fuelLevel;
        //setEfficiencyRatio();  pedig jó lenne ugy megcsinálni hogy amikor változik set-vel az üzemanyag automatikusna változik a maxDistance is
        //setMaxDistance();
    }

    public double setEfficiencyRatio() {
        if ((1.0 - (super.getKilometerCount() / 240000.0)) > 0.6) {
            return this.efficiencyRatio = 1.0 - (super.getKilometerCount() / 240000.0);
        } else {
            return this.efficiencyRatio = 0.6;
        }

    }

    @Override
    public String toString() {
        return "PetrolCar: " + super.toString() + "fuelLevel=" + fuelLevel + ", maxDistance=" + maxDistance;
    }
    
}
