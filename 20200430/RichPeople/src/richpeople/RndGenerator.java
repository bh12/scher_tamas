package richpeople;

public class RndGenerator {

    private static String[] firstName = {"Tamás", "Béla", "Gizella", "Tóbiás", "Jucus", "Anita"};
    private static String[] lastName = {"Kiss", "Nagy", "Kolompár", "Tóth", "Wick", "Hill"};

    public static int randomNumber(int max) {
        int randomNumber = (int) (Math.random() * max);
        return randomNumber;
    }

    public static String randomName() {
        int firstNameIndex = randomNumber(firstName.length);
        int lastNameIndex = randomNumber(lastName.length);
        String randomName = firstName[firstNameIndex] + lastName[lastNameIndex];
        return randomName;
    }

    public static int randomAge() {
        int randomAge = randomNumber(100) + 1;
        return randomAge;
    }

    public static int randomCountOfCoins() {
        int numberOfCoins = randomNumber(51);
        return numberOfCoins;
    }

}
