/*
Hozz létre egy Currency enumot. Tároljuk el benne a pénznemet (EUR, GBP, HUF, USD, JPN, RMB) és a forintbeli értékét integerként. 
Hozzunk létre egy Person osztályt is, amiben minden személynél eltárolju, hogy melyik pénznemből hány darab van neki. Tároljuk el még a személy korát és nevét is.
Generáljunk véletlenszerűen embereket különböző típusú és mennyiségű pénzzel. Írjuk ki a konzolra a következőket:
a.    az emberek vagyonának összértékét
b.    az emberek vagyonának átlagát
c.    az emberek vagyonának átlagát 18-49 korosztályban
d.    az emberek vagyonának medánját
e.    az emberek nevét vagyonát növekvő sorrendben
 */
package richpeople;

import java.util.ArrayList;
import java.util.List;

public class RichPeople {

    public static final int COUNT_OF_PERSONS = 10;
    public static List<Person> personsList = new ArrayList<>();

    public static void main(String[] args) {
        // TODO code application logic here

        //System.out.println(CurrencyEnum.EUR.getHufExchange());
        fillUpPersonsList();
        System.out.println(personsList.size());
      

    }

    public static void fillUpPersonsList() {
        Person tempPerson = new Person();
        for (int i = 1; i <= COUNT_OF_PERSONS; ++i) {
            tempPerson.setName(RndGenerator.randomName());
            tempPerson.setAge(RndGenerator.randomAge());
            tempPerson.setCountOfEUR(RndGenerator.randomCountOfCoins());
            tempPerson.setCountOfGBP(RndGenerator.randomCountOfCoins());
            tempPerson.setCountOfHUF(RndGenerator.randomCountOfCoins());
            tempPerson.setCountOfJPN(RndGenerator.randomCountOfCoins());
            tempPerson.setCountOfRMB(RndGenerator.randomCountOfCoins());
            
            personsList.add(tempPerson);

        }
    }

}
