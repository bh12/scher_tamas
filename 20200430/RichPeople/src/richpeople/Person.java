package richpeople;

public class Person implements Comparable<Person>{

    private String name;
    private int age;
    private int countOfEUR;
    private int countOfGBP;
    private int countOfHUF;
    private int countOfJPN;
    private int countOfRMB;

    public Person() {
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public int getCountOfEUR() {
        return countOfEUR;
    }

    public int getCountOfGBP() {
        return countOfGBP;
    }

    public int getCountOfHUF() {
        return countOfHUF;
    }

    public int getCountOfJPN() {
        return countOfJPN;
    }

    public int getCountOfRMB() {
        return countOfRMB;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setCountOfEUR(int countOfEUR) {
        this.countOfEUR = countOfEUR;
    }

    public void setCountOfGBP(int countOfGBP) {
        this.countOfGBP = countOfGBP;
    }

    public void setCountOfHUF(int countOfHUF) {
        this.countOfHUF = countOfHUF;
    }

    public void setCountOfJPN(int countOfJPN) {
        this.countOfJPN = countOfJPN;
    }

    public void setCountOfRMB(int countOfRMB) {
        this.countOfRMB = countOfRMB;
    }
    
    

    @Override
    public int compareTo(Person t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
