package richpeople;

public enum CurrencyEnum {

    EUR(355),
    GBP(408),
    HUF(1),
    USD(327),
    JPN(306),
    RMB(46);

    private int hufExchange;

    private CurrencyEnum(int hufExchange) {
        this.hufExchange = hufExchange;
    }

    public static CurrencyEnum getEUR() {
        return EUR;
    }

    public static CurrencyEnum getGBP() {
        return GBP;
    }

    public static CurrencyEnum getHUF() {
        return HUF;
    }

    public static CurrencyEnum getUSD() {
        return USD;
    }

    public static CurrencyEnum getJPN() {
        return JPN;
    }

    public static CurrencyEnum getRMB() {
        return RMB;
    }

    public int getHufExchange() {
        return hufExchange;
    }
    
    
    
    

}
