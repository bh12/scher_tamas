
package enumdays;

public enum DaysEnum {
    
    MONDAY("hétfőSzöveg"),
    TUESDAY("keddSzöveg"),
    WENSDAY("szerdaSzöveg"),
    THURSDAY("csütörtökSzöveg"),
    FRIDAY("péntekSzöveg"),
    SATURDAY("szombatSzöveg"),
    SUNDAY("vasárnapSzöveg");
    
    private final String text;

    private DaysEnum(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
    
    
    
    public void sayHello(DaysEnum days) {
        switch (days) {
            case MONDAY: System.out.println("Szia, " + this.MONDAY.getText()); break;
            case TUESDAY: System.out.println("Szia, " + this.TUESDAY.getText()); break;
            case WENSDAY: System.out.println("Szia, " + this.WENSDAY.getText()); break;
            case THURSDAY: System.out.println("Szia, " + this.THURSDAY.getText()); break;
            case FRIDAY: System.out.println("Szia, " + this.FRIDAY.getText()); break;
            case SATURDAY: System.out.println("Szia, " + this.SATURDAY.getText()); break;
            case SUNDAY: System.out.println("Szia, " + this.SUNDAY.getText()); break;
        }
        
            
        }
    }
    
    
    

