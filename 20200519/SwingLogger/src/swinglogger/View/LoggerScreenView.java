package swinglogger.View;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import swinglogger.Controller.LoggerController;

public class LoggerScreenView extends JFrame {

    LoggerController controller;

    private JPanel panel;
    private JTextArea text;
    private JButton button;

    public LoggerScreenView(LoggerController controller) {
        this.controller = controller;
    }

    public void init() {
        setUpWindow();

        setUpText();
        setUpButton();
        setLayout(new BorderLayout());

    }

    public void setUpWindow() {
        this.setVisible(true);
        this.setSize(400, 400);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setTitle("Idő rögzítő");
        
        setLayout(new GridLayout(0, 1));
        

        panel = new JPanel();
        add(panel);

    }

    public void setUpButton() {
        button = new JButton("Gomb");
        add(button, BorderLayout.WEST);
        this.button.addActionListener(event -> {
            controller.timeLogger();
            controller.writeIntoFile();

        });
        setVisible(true);

    }

    public void setUpText() {
        text = new JTextArea();
        add(text, BorderLayout.NORTH);
        setVisible(true);

    }

    public void setLoggedTimeToText(String text) {
        this.text.setText(text);
    }

    public JTextArea getText() {
        return text;
    }

    

}
