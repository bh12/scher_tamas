package swinglogger.Model;

import java.io.File;
import java.util.Date;
import java.util.LinkedList;

public class TimeLogged {
    
    LinkedList dateList;

    private Date loggedDate;
    
    public static File file = new File("logged_time.txt");
    

    public TimeLogged() {

    }

    public Date getLoggedDate() {
        return loggedDate;
    }

    public void setLoggedDate() {
        this.loggedDate = new Date();
    }

    @Override
    public String toString() {
        return "TimeLogged{" + "loggedDate=" + loggedDate + '}';
    }

}
