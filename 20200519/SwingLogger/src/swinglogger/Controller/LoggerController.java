package swinglogger.Controller;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import swinglogger.App;
import swinglogger.Model.TimeLogged;
import swinglogger.View.LoggerScreenView;

public class LoggerController {

    private LoggerScreenView view;
    private TimeLogged loggedTime;
    LinkedList<TimeLogged> dateList;

    public LoggerController() {
        this.loggedTime = new TimeLogged();
        this.view = new LoggerScreenView(this); //sztem a a loggerview-nak csinálok üres konstruktort akkor this nélkül is müködne
        this.view.init();
        this.dateList = App.dateList;
    }

    public void timeLogger() {
        this.loggedTime.setLoggedDate();
        String text = this.loggedTime.getLoggedDate().toString();
        this.view.setLoggedTimeToText(text);
        this.dateList.add(loggedTime);

    }

    public LinkedList<TimeLogged> getDateList() {
        return dateList;
    }
    
    public void writeIntoFile() {

        

        try (FileWriter fw = new FileWriter(TimeLogged.file)) {
            
            fw.write(this.loggedTime.getLoggedDate().toString());
            System.lineSeparator();
           

        } catch (IOException e) {
            System.out.println("File hiba");
            e.getStackTrace();
        }

    }
    
    

}
