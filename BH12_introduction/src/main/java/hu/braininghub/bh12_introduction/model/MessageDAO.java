package hu.braininghub.bh12_introduction.model;

import java.sql.SQLException;
import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Singleton
public class MessageDAO {

    @PersistenceContext
    private EntityManager em;

    public void addNewMessage(String email, String subject, String message) throws SQLException {

    }

    public Message getMessageById(int id) {
        Message m = em.find(Message.class, id);
        return m;
    }

    public List<MessageDTO> getMessages() throws SQLException {
        return null;
    }

}
