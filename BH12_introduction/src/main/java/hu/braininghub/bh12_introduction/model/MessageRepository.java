package hu.braininghub.bh12_introduction.model;

import java.util.ArrayList;
import java.util.List;

public class MessageRepository {
    private static List<MessageDTO> messages = new ArrayList();
    static {
        messages.add(new MessageDTO("valaki", "valami", "valamit"));
        messages.add(new MessageDTO("valaki", "valami", "valamit"));
        messages.add(new MessageDTO("valaki", "valami", "valamit"));
        messages.add(new MessageDTO("valaki", "valami", "valamit"));
    }
    public static void addMessage(MessageDTO message) {
        messages.add(message);
    }
        
    public static List<MessageDTO> getMessages() {
        return messages;
    }
    
    public static void removeMessage(MessageDTO message) {
        messages.remove(message);
    }
    
    public static void clearMessages() {
        messages.clear();
    }
}
