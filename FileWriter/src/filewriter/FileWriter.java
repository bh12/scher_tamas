
package filewriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;


public class FileWriter {

    public static void main(String[] args) {
        // TODO code application logic here
        
        Employee emp1 = new Employee("Tamás", 10, "0620123456", "Budapest");
        Employee emp2 = new Employee("Béla", 15, "0620123879", "Ecser");
        Employee emp3 = new Employee("Julis", 13, "062012389", "Győr");
        
        List<Employee> employeis = new ArrayList<>();
        employeis.add(emp1);
        employeis.add(emp2);
        employeis.add(emp2);
        
        objectWriter(employeis);
        
    }
    
    public static void objectWriter(List<Employee> employies) {
        
        try (FileOutputStream fos = new FileOutputStream("person.ser");
                ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            
            for (Employee emp : employies) {
                oos.writeObject(emp);
            }
            
            
        }
        catch (IOException iex) {
            iex.getLocalizedMessage();
            
        }

        
    }
    
}
