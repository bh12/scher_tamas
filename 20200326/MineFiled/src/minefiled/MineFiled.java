package minefiled;

import java.util.Scanner;

public class MineFiled {

    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner (System.in);
        int n = 5;
        int m = 5;
        int userLife = 2;

        int[][] mineFiled = mine2DArray(n, m);
        //print2DArray(mineFiled);

        int userGivenCoordinatN;
        int userGivenCoordinatM;
        int countOfneighbourMine;
        boolean isMine;

        for (; userLife > 0; userLife--) {

            do {
                coordinatRequest(n, m);
                userGivenCoordinatN = sc.nextInt();
                userGivenCoordinatM = sc.nextInt();
                isMine = mineDetector(mineFiled, userGivenCoordinatN, userGivenCoordinatM);
                if (isMine) {
                    System.out.printf("Aknára léptél, 1 élet elúszott, %d életed maradt", userLife - 1);
                    System.out.println("");
                } else {
                    countOfneighbourMine = neighbourMineCounter(mineFiled, userGivenCoordinatN, userGivenCoordinatM);
                    mineMessages(countOfneighbourMine);
                }

            } while (!isMine);

        }
        System.out.println("game over --- elfogyott az összes életed");

    }

    public static int[][] mine2DArray(int n, int m) {
        int[][] arr = new int[n + 2][m + 2];
        int countOfBombs = randomNumber(n * m / 4, n * m / 2);
        int placedBombCounter = 0;

        while (placedBombCounter != countOfBombs) {
            int i = randomNumber(1, n);
            int j = randomNumber(1, m);
            if (arr[i][j] == 0) {
                arr[i][j] = 1;
                placedBombCounter++;
            }
        }

        return arr;
    }

    public static int randomNumber(int min, int max) {
        int randomNumber = (int) (Math.random() * (max - min + 1) + min);
        return randomNumber;
    }

    public static void print2DArray(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + ", ");
            }
        }
    }

    public static boolean mineDetector(int[][] arr, int userGivenCoordinatN, int userGivenCoordinatM) {
        boolean isMine = false;
        if (arr[userGivenCoordinatN][userGivenCoordinatM] == 1) {
            return true;
        }

        return isMine;
    }

    public static void coordinatRequest(int n, int m) {

        System.out.printf("Adj meg 2 kooridnátát %d és %d értékeken belül", n, m);
        System.out.println("");
        

    }

    public static int neighbourMineCounter(int[][] arr, int userGivenCoordinatN, int userGivenCoordinatM) {
        int neighbourMineCounter = 0;
        for (int i = userGivenCoordinatN - 1; i <= userGivenCoordinatN + 1; i++) {
            for (int j = userGivenCoordinatM - 1; j <= userGivenCoordinatM + 1; j++) {
                neighbourMineCounter += arr[i][j];
                neighbourMineCounter -= arr[userGivenCoordinatN][userGivenCoordinatM];
            }
        }
        return neighbourMineCounter;
    }

    public static void mineMessages(int neighbourMineCounter) {
        if (neighbourMineCounter <= 1) {
            System.out.println("Nincs para, a körületted lévő aknák száma: " + neighbourMineCounter);
        } else if (neighbourMineCounter <= 4) {
            System.out.println("Óvatosan, kezd melegedni a helyzet, a körületted lévő aknák száma: " + neighbourMineCounter);
        } else {
            System.out.println("Hurt Locker!! A körületted lévő aknák száma: " + neighbourMineCounter);
        }
    }

}
