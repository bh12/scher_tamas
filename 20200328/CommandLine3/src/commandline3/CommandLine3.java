/*
3.    Parancssorból kapott Stringeket vizsgáljuk meg, és írjuk ki azt, 
hogy „Malacod van!” ha van a paraméterek között „pig” érték.
 */
package commandline3;

public class CommandLine3 {

    public static void main(String[] args) {
        // TODO code application logic here

        String searchedText = "pig";

        for (int i = 0; i < args.length; i++) {
            if (args[i].equals(searchedText)) {
                System.out.println("Malacod van!");
            }
        }
    }

}
