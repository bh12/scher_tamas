/*
1.    Parnacssorból kapott egész számoknak írja ki a konzolra az összegüket.
 */
package commandline1;

public class CommandLine1 {

    public static void main(String[] args) {

        System.out.println("Parancsorból kapott számok:");
        for (int i = 0; i < args.length; i++) {
            System.out.print(args[i] + ", ");
        }
        System.out.println("");

        int sum = 0;

        for (int i = 0; i < args.length; i++) {
            sum += Integer.parseInt(args[i]);
        }

        System.out.println("Összegük: " + sum);

    }

}
