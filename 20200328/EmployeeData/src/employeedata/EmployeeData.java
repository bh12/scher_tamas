package employeedata;

public class EmployeeData {

    public static void main(String[] args) {
        // TODO code application logic here

        int[][][] employeeData = new int[10][12][3];
        //String firstName = "Laci, Józsi, Béla, Feri, Adri, Zsófi, Károly";
        //String lastName = "Nagy, Veréb, Molnár, Kiss, Papp, Tóth";
        //String[][] employeeName = new String[2][];
        //new employeeName[0][] = firstName.split(","); 
        
                
        for (int i = 0; i < employeeData.length; i++) {
            for (int j = 0; j < employeeData[i].length; j++) {

                employeeData[i][j][0] = i + 1;    // itt az azonosítoikat tároljuk el, most csak 1-től növelve
                employeeData[i][j][1] = 450000 + randomNumber(50000, 125000);
                employeeData[i][j][2] = j + 1;
                if (employeeData[i][j][2] % 3 == 0) {
                    employeeData[i][j][1] = (int) (employeeData[i][j][1] * (1 + randomNumber(5, 15) / 100.00));
                }
            }
        }

        print3DArray(employeeData);

    } // main vége

    public static String monthName(int name) {
        String name1 = "";
        switch (name) {
            case 1:
                System.out.print("Január    ");
                break;
            case 2:
                System.out.print("Február   ");
                break;
            case 3:
                System.out.print("Március   ");
                break;
            case 4:
                System.out.print("Április   ");
                break;
            case 5:
                System.out.print("Május     ");
                break;
            case 6:
                System.out.print("Juniús    ");
                break;
            case 7:
                System.out.print("Július    ");
                break;
            case 8:
                System.out.print("Augusztus ");
                break;
            case 9:
                System.out.print("Szeptember");
                break;
            case 10:
                System.out.print("Október   ");
                break;
            case 11:
                System.out.print("November  ");
                break;
            case 12:
                System.out.print("December  ");
                break;
        }
        return name1;
    }

    public static int randomNumber(int min, int max) {
        int randomNumber = (int) (Math.random() * (max - min + 1) + min);
        return randomNumber;
    }

    public static void print3DArray(int[][][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                //for (int z = 0; z < arr[i][j].length; z++) {
                System.out.print("Azonosító: " + arr[i][j][0] + " ");
                System.out.print(monthName(arr[i][j][2]));
                System.out.println(" havi bér: " + arr[i][j][1]);

            }
            System.out.println("");
        }

    }

}
