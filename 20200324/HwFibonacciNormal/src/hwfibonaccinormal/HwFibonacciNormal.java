/*
fibonacci n. elem meghatározása normál modszerrel
 */
package hwfibonaccinormal;

public class HwFibonacciNormal {

    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println(fibonacciNormal(8));
    }
    
    public static int fibonacciNormal(int a) {
        int n = 0;
        int n1 = 1;
        int counter = 0;
        
        while (counter != a) {
        n = n + n1;
        n1 = n - n1;
        counter++;
        }
        return n;
    }
    
}
