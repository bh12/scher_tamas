/*
 3. A program állítson elő véletlenszerűen két megegyező méretű mátrixot! 
Számítsa ki a és írja ki a két mátrix összegét! 
Minden részfeladatot külön metódus valósítson meg!
Nem irta a feladat de a véletlenszerű méretet csak 5 és 10 közé teszem hogy átlátható legyen, 
a feltötlés meg 0 és 10 közé eső véletlen számokkal lesz.
 */
package hw2matrix;

public class Hw2Matrix {

    public static void main(String[] args) {
        // TODO code application logic here
        int i = randomNumber(5, 10);
        int j = randomNumber(5, 10);

        int[][] arr1 = new int[i][j];
        int[][] arr2 = new int[i][j];

        fill2DArray(arr1);
        fill2DArray(arr2);

        int[][] sumArray = new int[i][j];
        sumOf2SameSizedMatrix(arr1, arr2, sumArray);
        print2DArray(sumArray);

    }

    public static int randomNumber(int min, int max) {
        int randomNumber = (int) (Math.random() * (max - min + 1) + min);
        return randomNumber;
    }

    public static int[][] fill2DArray(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = randomNumber(0, 10);
            }
        }
        return arr;
    }

    public static int[][] sumOf2SameSizedMatrix(int[][] arr1, int[][] arr2, int[][] sum) {
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr1[i].length; j++) {
                sum[i][j] = arr1[i][j] + arr2[i][j];
            }
        }
        return sum;
    }

    public static void print2DArray(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + ", ");
            }
        }
    }

}
