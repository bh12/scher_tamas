/*
 4. A program legyen képes ötös és hatos lottó egy lehetséges húzásának eredményét visszaadni. 
A program köszöntse a felhasználót, majd egy menüvel döntse el, 
hogy ötös vagy hatos lottó számokat adjon vissza (menü) 
 */
package hwlottomenu;

import java.util.Scanner;

public class HwLottoMenu {
public static final int EXIT_MENU_NUMBER = 3;

    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner (System.in);
        
        int number;
        do {
            System.out.println("Hello felhasználó, válsz az alábbi menüből:");
            System.out.println("1. 5-ös lotto tipp");
            System.out.println("2. 6-os lotto tipp");
            System.out.println(EXIT_MENU_NUMBER + ". kilépés");
            
            
            number = sc.nextInt();
            switch(number) {
                case 1: otosLotto(); break;
                case 2: hatosLotto(); break;
            } 
        } while (number != EXIT_MENU_NUMBER);
        
    }
    
    public static void otosLotto(){
             int[] randomLotto = new int[5];
        int temporaryLottoNum;

        for (int i = 0; i < randomLotto.length; i++) {
            do {
                temporaryLottoNum = (int) (Math.random() * (90 - 1 + 1) + 1);
            } while (temporaryLottoNum == randomLotto[0]
                    || temporaryLottoNum == randomLotto[1]
                    || temporaryLottoNum == randomLotto[2]
                    || temporaryLottoNum == randomLotto[3]
                    || temporaryLottoNum == randomLotto[4]);

            randomLotto[i] = temporaryLottoNum;
        }

        for (int i = 0; i < randomLotto.length; i++) {
            System.out.print(randomLotto[i] + ", ");
            System.out.println("");
        }
    }
    
     public static void hatosLotto(){
             int[] randomLotto = new int[6];
        int temporaryLottoNum;

        for (int i = 0; i < randomLotto.length; i++) {
            do {
                temporaryLottoNum = (int) (Math.random() * (45 - 1 + 1) + 1);
            } while (temporaryLottoNum == randomLotto[0]
                    || temporaryLottoNum == randomLotto[1]
                    || temporaryLottoNum == randomLotto[2]
                    || temporaryLottoNum == randomLotto[3]
                    || temporaryLottoNum == randomLotto[4]
                    || temporaryLottoNum == randomLotto[5]);

            randomLotto[i] = temporaryLottoNum;
        }

        for (int i = 0; i < randomLotto.length; i++) {
            System.out.print(randomLotto[i] + ", ");
            System.out.println("");
        }
    }
    
}
