/*
 1. A program dolgozzon egy 20 elemű tömbbel, töltse fel véletlen kétegyű számokkal! 
Minden részfeladatot külön metódusba kell megvalósítani
    - Listázza a tömbelemeket
    - Páros tömbelemek összege
    - Van-e öttel osztható szám
    - Melyik az első páratlan szám a tömbben
    - Van-e a tömbben 32
 */
package hw20elementarray;

public class Hw20ElementArray {

    public static void main(String[] args) {
        // TODO code application logic here
        int[] arr = new int[20];
        
        fill1DArray(arr);
        print1DArray(arr);
        
        System.out.println("");
        System.out.println("Páros tömbelemek összege: ");
        System.out.println(sumOfEvenItemsin1DArray(arr));
        
        System.out.print("Tartalmaz 5-vel osztható számot: ");
        System.out.println(divideabilityExaminationIn1DArray(arr, 5));
        
        System.out.print("Az első páratlan szám a ");
        System.out.println(indexSearchin1DArray(arr) + ". indexen van");
        
        System.out.print("A tömb tartalmazza a 32-es számot: ");
        System.out.println(isNumberIn1DArray(arr, 32));
       
        
    }

    public static int randomNumber(int min, int max) {
        int randomNumber = (int) (Math.random() * (max - min + 1) + min);
        return randomNumber;
    }

    public static int[] fill1DArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = randomNumber(10, 99);
        }
        return arr;
    }

    public static void print1DArray(int[] arr) {
        System.out.println("Tömb elemei: ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + ", ");
        }
    }

    public static int sumOfEvenItemsin1DArray(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0) {
                sum += arr[i];
            }
        }

        return sum;
    }

    public static boolean divideabilityExaminationIn1DArray(int[] arr, int divider) { 
   	boolean divideability = false;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % divider == 0) {
                divideability = true;
            }
        }
        return divideability;
    }

    public static int indexSearchin1DArray(int[] arr) {
        int searchedIndex = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 != 0) {
                searchedIndex = i;
                break;

            }
        }
        return searchedIndex;
    }

    public static boolean isNumberIn1DArray(int[] arr, int searchedNumber) {
	boolean isNumber = false;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % searchedNumber == 0) {
                isNumber = true;
                break;
            }
        }
        return isNumber;
    }

}
