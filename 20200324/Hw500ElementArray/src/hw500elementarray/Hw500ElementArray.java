/*
 2. A program töltsön fel véletlenszerűen kétjegyű számokkal egy 500 elemű tömböt, 
majd csináljon statisztikát róla, melyik szám hányszor fordul elő!
 */
package hw500elementarray;

public class Hw500ElementArray {

    public static void main(String[] args) {
        // TODO code application logic here

        int[] arr = new int[500];
        fill1DArray(arr);

        int[] statisticArr = new int[90];
        
        numberCounterIn1DArray(arr, statisticArr);
        print1DArray(statisticArr);
        
    }

    public static int randomNumber(int min, int max) {
        int randomNumber = (int) (Math.random() * (max - min + 1) + min);
        return randomNumber;
    }

    public static int[] fill1DArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = randomNumber(10, 99);
        }
        return arr;
    }

    public static void print1DArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.println(i + 10 +". szám " + arr[i] + " alkalommal került legenerálásra");
        }
    }

    public static int[] numberCounterIn1DArray(int[] arr, int[] arr1) {

        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (arr[j] == i + 10) {
                    arr1[i]++;
                }
            }
        }
        return arr1;
    }

}
