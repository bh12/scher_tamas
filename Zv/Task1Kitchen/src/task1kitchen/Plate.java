
package task1kitchen;


public class Plate extends KitchenTools {
    
    

    public Plate() {
        super.isDirty = StateOfKitchenTools.isDirty();
    }

    @Override
    public String toString() {
        return "Plate{" + "isDirty=" + isDirty + '}';
    }
    
    
    
}
