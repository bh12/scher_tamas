
package task1kitchen;

import java.util.ArrayList;
import java.util.List;


public class Task1Kitchen {
    
    public static List<KitchenTools> kitchenCounter = new ArrayList<>();
    public static List<KitchenTools> dishWasher = new ArrayList<>();
   
    public static void main(String[] args) {
        // TODO code application logic here
          
        fillUpKitchenCounter(100, 1, 100);

        
        try {
        
            Andras andras = Andras.getInstance();
            
            andras.kitchenOrderExamination(kitchenCounter, dishWasher);
            
        } catch (DishWasherIsFullExeption ex) {
            System.out.println("tele a gép");
            System.out.println(dishWasher.size());
            System.out.println(kitchenCounter.size());
        }

    }
    
    
    public static int randomNumber(int min, int max) {
        return (int) (Math.random() * (max - min + 1) + min);
    }
    
    public static void fillUpKitchenCounter(int count, int min, int max) {
        
        for (int i = 0; i < count; i++) {
            int rnd = randomNumber(min, max);
            KitchenTools kitchenTool;
            if (rnd <= 30) {
                
                kitchenTool = new Plate();
                
            } else {
                
                kitchenTool = new Glass();
                
            }
            
            kitchenCounter.add(kitchenTool);
            
        }
        
    }

    
}
