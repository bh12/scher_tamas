
package task1kitchen;

import java.util.List;



public class Andras {
    
    private static final Andras INSTANCE = new Andras();
    
    public static Andras getInstance() {
        return INSTANCE;
    }
    
    public static void kitchenOrderExamination(List<KitchenTools> kitchenCounter, List<KitchenTools> dishWasher) throws DishWasherIsFullExeption {
        
        KitchenTools toolToBeChecked;
        
        //for (KitchenTools tool : kitchenCounter) {
        for (int i = 0; i < kitchenCounter.size(); i++) {
            
        
            if (kitchenCounter.get(i).isIsDirty()) {
                //kitchenCounter.remove(tool);
                dishWasher.add(kitchenCounter.get(i));
                kitchenCounter.remove(kitchenCounter.get(i));
                if (dishWasher.size() == 15) {
                    throw new DishWasherIsFullExeption();
                }
            }
        }
    }
    
}
