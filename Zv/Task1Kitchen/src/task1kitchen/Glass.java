
package task1kitchen;

import com.sun.beans.util.Cache;


public class Glass extends KitchenTools {
    
    

    public Glass() {
        super.isDirty = StateOfKitchenTools.isDirty();
    }
    
    

    @Override
    public String toString() {
        return "Glass{" + "isDirty=" + isDirty + '}';
    }
    
    
    
    
    
    
}
