
package task1kitchen;


public abstract class KitchenTools implements StateOfKitchenTools{
    
    boolean isDirty;

    public boolean isIsDirty() {
        return isDirty;
    }

    public void setIsDirty(boolean isDirty) {
        this.isDirty = isDirty;
    }
    
    
    
}
