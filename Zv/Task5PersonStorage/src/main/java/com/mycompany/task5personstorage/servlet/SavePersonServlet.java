
package com.mycompany.task5personstorage.servlet;

import com.mycompany.task5personstorage.services.PersonService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "SavePersonServlet", urlPatterns = {"/SavePersonServlet"})
public class SavePersonServlet extends HttpServlet {
    
    @Inject
    PersonService personService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("WEB-INF/personStorage.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        String name = req.getParameter("name");
        int age = Integer.valueOf(req.getParameter("age"));
        String email = req.getParameter("email");
        
        String emailRegex = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
    
        if (email.matches(emailRegex)) {
        
        personService.savePerson(name, age, email);
        
        } else {
            
            req.getRequestDispatcher("WEB-INF/personStorage.jsp").forward(req, resp);
            
        }
        
    }

   

}
