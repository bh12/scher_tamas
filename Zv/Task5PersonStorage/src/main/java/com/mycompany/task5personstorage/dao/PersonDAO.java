
package com.mycompany.task5personstorage.dao;

import com.mycompany.task5personstorage.entities.PersonEntity;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Singleton
public class PersonDAO {
    
      @PersistenceContext
    private EntityManager em;
      
      public void savePerson(String name, int age, String email) {
          PersonEntity person = new PersonEntity();
          person.setName(name);
          person.setAge(age);
          person.setEmil(email);
          
          em.persist(person);
      }
    
}
