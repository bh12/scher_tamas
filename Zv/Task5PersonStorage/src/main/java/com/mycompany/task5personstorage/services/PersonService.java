
package com.mycompany.task5personstorage.services;

import com.mycompany.task5personstorage.dao.PersonDAO;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.inject.Inject;

@Singleton
@TransactionAttribute
public class PersonService {
    
    @Inject
    PersonDAO personDAO;
    
    public void savePerson(String name, int age, String email) {
        personDAO.savePerson(name, age, email);
        
    }
    
}
