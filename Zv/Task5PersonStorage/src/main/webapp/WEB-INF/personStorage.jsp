<%-- 
    Document   : personStorage
    Created on : Sep 26, 2020, 12:23:11 PM
    Author     : scher
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Person Storage</title>
    </head>
      <body>
        <form method="POST" action="SavePersonServlet">
            
                <div class="form-group">
                    <label for="name">name:</label>
                    <input type="text" class="form-control" placeholder="Enter name" name="name" id="name">
                </div>
            
                <div class="form-group">
                    <label for="age">age:</label>
                    <input type="text" class="form-control" placeholder="Enter age" name="age" id="age">
                </div>
            
            <div class="form-group">
                    <label for="email">email:</label>
                    <input type="text" class="form-control" placeholder="Enter email" name="email" id="email">
                </div>
            
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
    </body>
</html>
