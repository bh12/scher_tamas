
package task2filehandling;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class Task2FileHandling {
    
    public static List<Fruit> fruits = new ArrayList<>();

  
    public static void main(String[] args) {
        // TODO code application logic here
        
        txtReader();
        txtWriter();
        
        
    }
    
      public static void txtReader() {

        try (BufferedReader br = new BufferedReader(new FileReader("pelda.txt"))) {

            String line;

            while ((line = br.readLine()) != null) {

                String[] values = line.split(";");
                Fruit fruit = new Fruit();
                fruit.setName(values[0]);
                fruit.setKg(Integer.valueOf(values[1]));
                fruit.setPrice(Integer.valueOf(values[2]));

                fruits.add(fruit);
            }

        } catch (IOException ex) {
            System.out.println("File hiba");

        }

    }
      
      public static List<String> totalPriceCalc() {
        
        List<String> data = fruits.stream().map((Fruit f) -> {int totalPirce = (f.getPrice() * f.getKg());
                                                    String name = f.getName();
                                                    String totalPriceWithName = name + ";" + totalPirce;
                                                    return totalPriceWithName;
        
        }).collect(Collectors.toList());
              
        return data;
    }
      
      public static void txtWriter() {
        
        try (BufferedWriter bw = new BufferedWriter(new FileWriter("total_price.txt"))) {
            
            List<String> data = totalPriceCalc();
            for (String line : data) {
                bw.write(line);
                bw.newLine();
                bw.flush();
                
            }
            
            
        } catch (IOException exp) {
            System.out.println("kiiráskori file hiba");
        }
        
    }
    
}
