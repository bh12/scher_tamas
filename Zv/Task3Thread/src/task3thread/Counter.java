
package task3thread;

import java.util.concurrent.atomic.AtomicInteger;


public class Counter implements Runnable{
    
    private static AtomicInteger counter = new AtomicInteger(0);

    @Override
    public void run() {
        
        int countPerThread = 50;
        
        for (int i = 0; i < countPerThread; i++) {
            int rnd = randomNumber(1, 100);
            counter.addAndGet(rnd);
            System.out.println(Thread.currentThread().getName() + ": " + counter);
        }
        
    }
    
    public static int randomNumber(int min, int max) {
        return (int) (Math.random() * (max - min + 1) + min);
    }

    
    
    
}
