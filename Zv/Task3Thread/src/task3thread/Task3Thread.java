
package task3thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Task3Thread {
    
    private static final int THREAD_POOL_SIZE = 5;

    public static void main(String[] args) {
        // TODO code application logic here
        
        ExecutorService se = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
        
        for (int i = 0; i < THREAD_POOL_SIZE; i++) {
        
            Counter task = new Counter();
            se.submit(task);
                    
        }
        
        se.shutdown();
    }
    
}
