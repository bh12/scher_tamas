
package storageapp;

import java.time.LocalDate;

public abstract class Product implements Comparable<Product>{
    
    private ProductType type;
    private String producer;
    private int barCode;
    private int price;
    private LocalDate registrationDate;

    public Product(ProductType type, String producer, int barCode, int price) {
        this.type = type;
        this.producer = producer;
        this.barCode = barCode;
        this.price = price;
    }
    
    

    public ProductType getType() {
        return type;
    }

    public void setType(ProductType type) {
        this.type = type;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public int getBarCode() {
        return barCode;
    }

    public void setBarCode(int barCode) {
        this.barCode = barCode;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }
    
    

    @Override
    public int compareTo(Product t) {
        return this.getBarCode() - t.getBarCode();
    }

    
    
}
