
package storageapp;

public class BeautyProduct extends Product{
    
    private int weightInKg;

    public BeautyProduct(ProductType type, String producer, int barCode, int price) {
        super(type, producer, barCode, price);
    }

    public int getWeightInKg() {
        return weightInKg;
    }

    public void setWeightInKg(int weightInKg) {
        this.weightInKg = weightInKg;
    }

   

    
    
    
    
}
