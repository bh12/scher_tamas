package storageapp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.Scanner;

public class ProductStorage {

    public final Scanner sc = new Scanner(System.in);

    // ezt nem igy kéne, a kacsacsörbe máshogy kéne
    private ArrayList<Product> listOfProduct; // mégis igy kell olyat nem tudnék csinálni h a 3 különféle utodosztályokat pakolgatom bele

    public void add() {

        System.out.println("Add meg az adatokat veszővel elválasztva");
        String data = sc.next();
        String[] datasForNewProduct = data.split(",");

        ProductType type = ProductType.valueOf(datasForNewProduct[0]); //itt nem tudom h lehetne azt nézni hogy kisbetű nagybetű
        String producer = datasForNewProduct[1];
        int barCode = Integer.parseInt(datasForNewProduct[2]);
        int price = Integer.parseInt(datasForNewProduct[3]);
        String productClass = datasForNewProduct[4];

        switch (productClass) {
            case "KitchenProduct":
                Product kitchenProduct = new KitchenProduct(type, producer, barCode, price);
                listOfProduct.add(kitchenProduct);
                break;

            case "EntertainmentProduct":
                Product entertainmentProduct = new EntertainmentProduct(type, producer, barCode, price);
                listOfProduct.add(entertainmentProduct);
                break;

            case "BeautyProduct":
                Product beautyProduct = new BeautyProduct(type, producer, barCode, price);
                listOfProduct.add(beautyProduct);
                break;

        }

    }

    public void remove() {
        System.out.println("Add meg az eltávolítani kívánt termék vonalkódját");

        int barCode = Integer.parseInt(sc.next());
        final Optional<Product> productToRemove = listOfProduct.stream()
                .filter(product -> product.getBarCode() == barCode)
                .findAny();

        listOfProduct.remove(productToRemove);

    }

    public void report() {
        int numberOfProductInStorage = listOfProduct.size();
        System.out.println("Készleten lévő termékek száma: " + numberOfProductInStorage + " db");
        
        listOfProduct.stream().
                filter(product -> EntertainmentProduct.class.equals(product.getClass()))
                .map(product -> (EntertainmentProduct) product)
                .filter(prd -> prd.getCountOfSwtichedIn() >= 2)
                .count();

    }
    
    // castolást igy probálni .filter{t->(interface)x = t; x.swicthcount>2)

}
