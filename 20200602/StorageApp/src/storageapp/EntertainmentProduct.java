
package storageapp;

public class EntertainmentProduct extends Product implements Switchable{
    
    private int countOfSwtichedIn;

    public EntertainmentProduct(ProductType type, String producer, int barCode, int price) {
        super(type, producer, barCode, price);
    }

    public int getCountOfSwtichedIn() {
        return countOfSwtichedIn;
    }

    public void setCountOfSwtichedIn(int countOfSwtichedIn) {
        this.countOfSwtichedIn = countOfSwtichedIn;
    }
    
    
    
    
    
}
