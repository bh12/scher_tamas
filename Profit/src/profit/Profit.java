
package profit;


public class Profit {

    //public static int[] array = {30,5,14,23};
    
    public static void main(String[] args) {
        
        int[] exampleArray = {30,5,14,23};
      
        maxProfit(exampleArray);
        
    }
    
       public static int minElementIndex(int[] array) {
        int min = array[0];
        int minIndex = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
                minIndex = i;
            }
            
        }
        return minIndex;
    }
       
       public static int[] elementsAfterMinElement(int[] array) {
           int minIndex = minElementIndex(array);
           int sizeOfArray = array.length - minIndex;
           
           int[] elementsAfterMinElement = new int[sizeOfArray];
           
           for (int i = 0; i < elementsAfterMinElement.length; i++) {
               elementsAfterMinElement[i] = array[minIndex + i];
               
           }
           
           return elementsAfterMinElement;
       }
       
         public static int maxElement(int[] array) {
        int max = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
            
        }
        return max;
    }
         
         public static void maxProfit(int[] array) {
             
             int maxProfit = 0;
             
             int maxElement = maxElement(elementsAfterMinElement(array));
             int minElement = array[minElementIndex(array)];
             
             if (maxElement < minElement) {
                 System.out.println("Nincs profit");
             }
             else {
                 System.out.println("Max profit: ");
                 System.out.println(maxElement - minElement);
             }
             
             
         }


    
}
