/*
1.Írjuk ki egy fájl tulajdonságait (fájl-e?, útvonal, abszolút útvonal, szülőmappa, 
fájl neve, mérete, olvasható-e, írható-e, rejtett-e, utolsó módosítás dátuma)
*/
package filehandling;

import java.io.File;
import java.util.Date;

public class FileHandling {

    public static void main(String[] args) {
        // TODO code application logic here
        File f = new File("sample.txt");
        
        System.out.println("Abszolút útvonal: " + f.getAbsolutePath());
        System.out.println("Útvonal: " + f.getPath());
        System.out.println("Fájl-e: " + f.isFile());
        System.out.println("Rejtett-e: " + f.isHidden());
        System.out.println("Neve: " + f.getName());
        System.out.println("Szülőmappa: " + f.getParent());
        System.out.println("Olvasható-e: " + f.canRead());
        System.out.println("Irható-e: " + f.canWrite());
        Date date = new Date(f.lastModified());
        System.out.println(date);
        
       
        
        
    }
    
}
