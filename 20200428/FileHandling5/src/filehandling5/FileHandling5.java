/*
5.A program kérjen be egy elérési és egy cél útvonalat. Az elérési útvonalon lévő fájlt 
tartalmával együtt másolja át a cél útvonalra. 
Ha a megadott fájl egy mappa, akkor dobjunk kivételt amit le is kezelünk. 
Ha bármi hiba történik (például a cél útvonal ugyanaz mint az elérési útvonal) 
akkor is dobjunk kivételt, és kezeljük le.

Másolandó file: /home/scher/Desktop/Programozas/forraskod/scher_tamas/20200428/FileHandling5/tobecopy.txt
Másolni: /home/scher/Desktop/Programozas/forraskod/scher_tamas/20200428/FileHandling5/target/copied.txt
 */
package filehandling5;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FileHandling5 {

    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);

        String sourcePath = sc.nextLine();
        String targetPath = sc.nextLine();

        File sourceFile = new File(sourcePath);
        File targetFile = new File(targetPath);

        char[] charBuffer = new char[10];

        try (BufferedReader br = new BufferedReader(new FileReader(sourceFile));
                FileWriter fw = new FileWriter(targetFile)) {

            String line = br.readLine();

            while (line != null) {
                System.out.println(line);
                fw.write(line);
                line = br.readLine();
                
            }

        } catch (IOException e) {
            System.out.println("File hiba");
        }
    }

}
