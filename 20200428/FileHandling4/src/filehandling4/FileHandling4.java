/*
4.A program olvassa be a „scanner_text.txt” fájl tartalmát és írja ki a konzolra.
 */
package filehandling4;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FileHandling4 {

    public static void main(String[] args) {
        // TODO code application logic here

        File f = new File("scanner_text.txt");

        char[] charBuffer = new char[10];

        try (FileReader fr = new FileReader(f)) {
            
            int readCount = fr.read(charBuffer);
            
            while (readCount != -1) {
                System.out.print(new String(charBuffer, 0, readCount));
                readCount = fr.read(charBuffer);
            }

        } catch (IOException e) {
            System.out.println("File hiba");
        }
    }

}
