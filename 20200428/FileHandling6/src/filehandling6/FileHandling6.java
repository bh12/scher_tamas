/*
6.Készítsen lottó szelvény készítő programot. A program a felhasználó által megadott 
darabszámú lottó szelvényt generáljon, és mindet egy külön fájlba írja ki olyan formátumban, 
hogy majd ezt később be is tudjuk olvasni

MÉG JAVÍTANI hogy uj sorba menjen a szelvény meg legyen sorszáma is.
meg a randomLotto while feltételét szépre csinálni

 */
package filehandling6;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FileHandling6 {

    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        System.out.println("Hány lotto szelvény generálodjon?");
        int lottoTicketCount = sc.nextInt();
        int counter = 0;

        int[] lotto = new int[5];
        String line = "szelvény;";
        StringBuilder builder = new StringBuilder();

        do {

            builder.append(line);

            randomLotto(lotto);

            for (int i = 0; i < lotto.length; i++) {

                builder.append(lotto[i]);
                builder.append(";");

            }

            try (FileWriter fw = new FileWriter("lotto.txt")) {
                fw.write(builder.toString() + "\n");
            } catch (IOException e) {
                
                System.out.println("File hiba");
            }
            
            System.out.println(++counter);

        } while (counter != lottoTicketCount);

    }

    public static int[] randomLotto(int[] randomLotto) {
        int temporaryLottoNum;
        for (int i = 0; i < randomLotto.length; i++) {

            do {
                temporaryLottoNum = (int) (Math.random() * (90 - 1 + 1) + 1);
            } while (temporaryLottoNum == randomLotto[0]
                    || temporaryLottoNum == randomLotto[1]
                    || temporaryLottoNum == randomLotto[2]
                    || temporaryLottoNum == randomLotto[3]
                    || temporaryLottoNum == randomLotto[4]);

            randomLotto[i] = temporaryLottoNum;
        }

        return randomLotto;
    }

}
