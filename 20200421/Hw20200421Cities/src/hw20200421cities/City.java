
package hw20200421cities;

import java.util.Objects;

public class City {
    
    private String nameOfCity;
    private long populationOfCity;

    public City(String nameOfCity, long populationOfCity) {
        this.nameOfCity = nameOfCity;
        this.populationOfCity = populationOfCity;
    }

    public String getNameOfCity() {
        return nameOfCity;
    }

    public long getPopulationOfCity() {
        return populationOfCity;
    }

    public void setNameOfCity(String nameOfCity) {
        this.nameOfCity = nameOfCity;
    }

    public void setPopulationOfCity(long populationOfCity) {
        this.populationOfCity = populationOfCity;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.nameOfCity);
        hash = 47 * hash + (int) (this.populationOfCity ^ (this.populationOfCity >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final City other = (City) obj;
        if (this.populationOfCity != other.populationOfCity) {
            return false;
        }
        if (!Objects.equals(this.nameOfCity, other.nameOfCity)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nameOfCity + ", lakosság: " + populationOfCity;
    }
    
    

   
    
    
    
}
