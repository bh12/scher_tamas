
package hw20200421cities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Hw20200421Cities {

    public static void main(String[] args) {
        // TODO code application logic here
        
        Collection<City> hungaryCities = new ArrayList<>();
        hungaryCities.add(new City("Füred", 15000));
        hungaryCities.add(new City("Dörgicse", 500));
        hungaryCities.add(new City("Pest", 1000000));
        
        Hungary hungary = new Hungary(hungaryCities);
        System.out.println(hungary + " population: " + hungary.populationOfCountry(hungaryCities));
       
        Collection<City> operenciasCities = new ArrayList<>();
        operenciasCities.add(new City("Üveghegy", 150));
        operenciasCities.add(new City("Zsákfalu", 50));
        operenciasCities.add(new City("Megapolis", 10000000));
        
        Operencias operencias = new Operencias(operenciasCities);
        System.out.println(operencias + " population: " + operencias.populationOfCountry(operenciasCities));
        
        
        
        Set<Country> countryHashList = new HashSet<>();
        countryHashList.add(hungary);
        countryHashList.add(operencias);
              
        System.out.println(countryHashList.size());
        
        /* ezt itt benéztem, valahogy az országokat máshogy kellett volna kezelnem,
        vagy ha az összhasonlitáshoz lehetne valahogy egy Hungary típust contryvá castolni az lehet,
        hogy segítene cska kifutottam az időből :(
        Set<Country> countryTreeList = new TreeSet<>();
        System.out.println(countryTreeList.add(operencias));
        System.out.println(countryTreeList.add(hungary));
        
        
        System.out.println(countryTreeList.size());
        
        for (Country country : countryTreeList) {
            System.out.println(country + ", ");
        } 

        */
        
        
     
       
    }
    
}
