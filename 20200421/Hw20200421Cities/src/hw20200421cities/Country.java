package hw20200421cities;

import java.util.Collection;
import java.util.Iterator;

public abstract class Country {

    protected long populationOfCountry;
    protected Collection<City> cities;

    public long populationOfCountry(Collection<City> cities) {
        populationOfCountry = 0;
        final Iterator<City> cityIterator = cities.iterator();
        while (cityIterator.hasNext()) {
            populationOfCountry += cityIterator.next().getPopulationOfCity();
        }

        return populationOfCountry;

    }

    /*
    @Override
    public int compare(Country t, Country t1) {
        if (t.populationOfCountry(cities) > t1.populationOfCountry(cities)) {
            return -1;
        } else if (t.populationOfCountry(cities) == t1.populationOfCountry(cities)) {
            return 0;
        } else {
            return 1;
        }

    }
     */
    
    /*
    @Override
    public int compareTo(Country t) {
        if (this.populationOfCountry(cities) > t.populationOfCountry(cities)) {
            return -1;
        } else if (this.populationOfCountry(cities) == t.populationOfCountry(cities)) {
            return 0;
        } else {
            return 1;
        }
    }

*/

}
