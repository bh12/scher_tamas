<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"> 
        <title>Sakktábla</title>



        <style type="text/css">

            .page {
            }

            .chessboard {
                float: left;
                width: 480px;
                height: 480px;
                margin: 20px;
                border: 25px solid #333;
            }
            .black {
                float: left;
                width: 60px;
                height: 60px;
                background-color: #999;
                font-size:50px;
                text-align:center;
                display: table-cell;
                vertical-align:middle;
            }
            .white {
                float: left;
                width: 60px;
                height: 60px;
                background-color: #fff;
                font-size:50px;
                text-align:center;
                display: table-cell;
                vertical-align:middle;
            }

            .handlingField {
                float: left;

            }

            .movmentHandlingField {
                float: left;

            }

            .pageNavigatorField {
                float: left;

            }

        </style>

    </head>

    <body>

        <div class="page">

            <div class="chessboard">
                <!-- 1st -->
                <c:forEach items="${chessTableArray}" var="row" varStatus="rowIndex">
                    <c:forEach items="${row}" var="cell" varStatus="colIndex">

                        <c:choose>

                            <c:when test="${rowIndex.index % 2 == 0 && colIndex.index % 2 == 0}">

                                <div class="white">${cell}</div>

                            </c:when>

                            <c:when test="${rowIndex.index % 2 != 0 && colIndex.index % 2 != 0}">

                                <div class="white">${cell}</div>

                            </c:when>

                            <c:otherwise>
                                <div class="black">${cell}</div>
                            </c:otherwise>

                        </c:choose>
                    </c:forEach>
                </c:forEach>
            </div>

            <div class="handlingFiled">

                <div class="movmentHandlingField">
                    <form method="POST" action="chessGameServlet">
                        <label>Honnan:</label>

                        <select name="actualPosX" id="actualPosX">
                            <option value=""></option>
                            <option value="a">a</option>
                            <option value="b">b</option>
                            <option value="c">c</option>
                            <option value="d">d</option>
                            <option value="e">e</option>
                            <option value="f">f</option>
                            <option value="g">g</option>
                            <option value="h">h</option>
                        </select>
                        
                        
                        <input type="submit" value="Submit">
                    </form>
                </div>

            </div>

            <div class="pageNavigatorField"><%@include file="menu.jsp" %></div>

        </div>
    </body>
</html>
