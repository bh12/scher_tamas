<%-- 
    Document   : index
    Created on : Aug 20, 2020, 7:33:54 AM
    Author     : scher
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
        <%@include file="WEB-INF/head.jsp" %>
        
    </head>
    <body>
        <h1>Hello World!</h1>
        <div><%@include file="WEB-INF/menu.jsp" %></div>
    </body>
</html>
