
package com.mycompany.jatekprojekthez.service;

import com.mycompany.jatekprojekthez.modell.ChessPiece;
import java.util.ArrayList;
import java.util.List;


public class StartingPosSingeltonInit {
    
    private static final StartingPosSingeltonInit startPos = new StartingPosSingeltonInit();
        private static List<ChessPiece> chessPieceContainer;

    public StartingPosSingeltonInit() {
        chessPieceContainer = new ArrayList<>();
        initStartPos();
        
    }

    public static List<ChessPiece> getChessPieceContainer() {
        return chessPieceContainer;
    }
    
    
        
          
    private void initStartPos() {
    
    
     ChessPiece chessPiece1 = new ChessPiece('a', 1);
        ChessPiece chessPiece2 = new ChessPiece('b', 1);
        ChessPiece chessPiece3 = new ChessPiece('c', 1);
        ChessPiece chessPiece4 = new ChessPiece('e', 1);
        ChessPiece chessPiece5 = new ChessPiece('d', 1);
        ChessPiece chessPiece6 = new ChessPiece('f', 1);
        ChessPiece chessPiece7 = new ChessPiece('c', 1);
        
          chessPieceContainer.add(chessPiece1);
        chessPieceContainer.add(chessPiece2);
        chessPieceContainer.add(chessPiece3);
        chessPieceContainer.add(chessPiece4);
        chessPieceContainer.add(chessPiece5);
        chessPieceContainer.add(chessPiece6);
        chessPieceContainer.add(chessPiece7);
    
    }
    
}
