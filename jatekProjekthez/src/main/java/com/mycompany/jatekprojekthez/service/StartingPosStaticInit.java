
package com.mycompany.jatekprojekthez.service;

import com.mycompany.jatekprojekthez.modell.ChessPiece;
import java.util.ArrayList;
import java.util.List;


public class StartingPosStaticInit {
    
    private static final List<ChessPiece> chessPieceContainer;
    
    static {
    chessPieceContainer = new ArrayList<>();
    
     ChessPiece chessPiece1 = new ChessPiece('a', 7);
        ChessPiece chessPiece2 = new ChessPiece('b', 2);
        ChessPiece chessPiece3 = new ChessPiece('c', 1);
        ChessPiece chessPiece4 = new ChessPiece('e', 5);
        ChessPiece chessPiece5 = new ChessPiece('d', 5);
        ChessPiece chessPiece6 = new ChessPiece('f', 5);
        ChessPiece chessPiece7 = new ChessPiece('c', 5);
        
          chessPieceContainer.add(chessPiece1);
        chessPieceContainer.add(chessPiece2);
        chessPieceContainer.add(chessPiece3);
        chessPieceContainer.add(chessPiece4);
        chessPieceContainer.add(chessPiece5);
        chessPieceContainer.add(chessPiece6);
        chessPieceContainer.add(chessPiece7);
    
    }

    public static List<ChessPiece> getChessPieceContainer() {
        return chessPieceContainer;
    }
    
    
    
}
