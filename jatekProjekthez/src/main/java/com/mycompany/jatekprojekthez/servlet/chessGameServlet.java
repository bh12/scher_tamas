package com.mycompany.jatekprojekthez.servlet;

import com.mycompany.jatekprojekthez.util.EmailSend;
import com.mycompany.jatekprojekthez.modell.ChessPiece;
import com.mycompany.jatekprojekthez.service.StartingPosSingeltonInit;
import com.mycompany.jatekprojekthez.service.StartingPosStaticInit;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "chessGameServlet", urlPatterns = {"/chessGameServlet"})
public class chessGameServlet extends HttpServlet {

    
    
    //evel a doGettel ki akarom probálni a singelton incializálást (eager módban)
        @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        
       List<ChessPiece> container = StartingPosSingeltonInit.getChessPieceContainer();

        ChessPiece[][] chessTableArray = new ChessPiece[8][8];

        int asciiValueModificator = 97;

      
        
        while (!container.isEmpty()) {

            int lastIndexInArrayList = container.size() - 1;
            ChessPiece searchedGamePiece = container.get(lastIndexInArrayList);
            int secondArrayIndex = searchedGamePiece.getPosX() - asciiValueModificator;
            int firstArrayIndex = Math.abs(searchedGamePiece.getPosY() - 8);
            chessTableArray[firstArrayIndex][secondArrayIndex] = searchedGamePiece;
            container.remove(lastIndexInArrayList);

        }


        req.setAttribute("chessTableArray", chessTableArray);

        req.getRequestDispatcher("WEB-INF/sakk.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        char actualPosX = req.getParameter("actualPosX").charAt(0);
        
        System.out.println("actualPosX: " + actualPosX);
        
           final String fromEmail = "kuiters.java@gmail.com"; //requires valid gmail id
		final String password = "kuiters2020"; // correct password for gmail id
		final String toEmail = "tamas.scher@gmail.com"; // can be any email id 
		
		System.out.println("TLSEmail Start");
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
		props.put("mail.smtp.port", "587"); //TLS Port
		props.put("mail.smtp.auth", "true"); //enable authentication
		props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
		
                //create Authenticator object to pass in Session.getInstance argument
		Authenticator auth = new Authenticator() {
			//override the getPasswordAuthentication method
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, password);
			}
		};
		Session session = Session.getInstance(props, auth);
		
		EmailSend.sendEmail(session, toEmail,"TLSEmail Testing Subject", "TLSEmail Testing Body");
		
	
	}
        
    }
    
    
    


