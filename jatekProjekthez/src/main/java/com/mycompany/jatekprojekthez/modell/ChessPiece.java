package com.mycompany.jatekprojekthez.modell;

public class ChessPiece {

    private char posX;
    private int posY;

    public ChessPiece(char posX, int posY) {
        this.posX = posX;
        this.posY = posY;
    }

    public char getPosX() {
        return posX;
    }

    public void setPosX(char posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    @Override
    public String toString() {
        return "&#9820";
     //return String.valueOf(getPosX()) + getPosY();
    }

    

}
