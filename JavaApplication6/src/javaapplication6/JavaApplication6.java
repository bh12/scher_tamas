
package javaapplication6;

public class JavaApplication6 {

    
    public static void main(String[] args) {
        // TODO code application logic here
        int[][] chessTableArray =new int[8][8];
        for (int i = 0; i < chessTableArray.length; i++) {
            for (int j = 0; j < chessTableArray[i].length; j++) {
                chessTableArray[i][j] = (i+1) *(j+1);
                
            }
            
        }
        
        print2DArray(chessTableArray);
    }
    
    public static void print2DArray(int[][] array2D) {
        for (int i = 0; i < array2D.length; i++) {
            for (int j = 0; j < array2D[i].length; j++) {
                System.out.print(array2D[i][j] + ", ");

            }
            System.out.println("");

        }
    }

    
}
