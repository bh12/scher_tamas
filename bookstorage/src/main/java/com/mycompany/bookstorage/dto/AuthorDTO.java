
package com.mycompany.bookstorage.dto;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


public class AuthorDTO implements Serializable {
    
    @Id
    private long id;
    
    @Column
    private String name;
    
}
