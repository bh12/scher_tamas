
package com.mycompany.bookstorage.bean;

import com.mycompany.bookstorage.dto.BookDTO;
import java.util.List;


public class ListBooksPageBean {
    
    List<BookDTO> books;

    public List<BookDTO> getBooks() {
        return books;
    }

    public void setBooks(List<BookDTO> books) {
        this.books = books;
    }
    
    
    
}
