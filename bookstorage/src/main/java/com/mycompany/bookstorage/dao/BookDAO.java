
package com.mycompany.bookstorage.dao;

import com.mycompany.bookstorage.dto.BookFilterDTO;
import com.mycompany.bookstorage.entity.BookEntity;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Singleton
@TransactionAttribute
public class BookDAO {
    
    private static final String QUERY_FIND_BY_FILTER = "select b from BookEntity b";
    
    @PersistenceContext
    private EntityManager em;
    
    public List<BookEntity> findAll() {
        return em.createNamedQuery(BookEntity.QUERY_FIND_ALL, BookEntity.class).getResultList();
    }
    
    public List<BookEntity> findByFilter(BookFilterDTO filter) {
        
        if (filter.getAuthorName() == null && filter.getDescription() == null && filter.getIsbn() == null && filter.getTitle() == null)
            return findAll();
        
        String query = QUERY_FIND_BY_FILTER;
        if (filter.getDescription()!= null) {
            query += " where description like '%" + filter.getDescription() + "%'";
        }
        
        return em.createQuery(query, BookEntity.class).getResultList();
    }
    
    public void save(BookEntity bookEntity) {
        em.persist(bookEntity);
    }
}
