
package com.mycompany.bookstorage.exeption;


public class WrongBookTitleExeption extends RuntimeException{
    
    public WrongBookTitleExeption(String message) {
        super(message);
    }
    
}
