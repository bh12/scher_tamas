
package com.mycompany.bookstorage.servlet;

import com.mycompany.bookstorage.bean.ListBooksPageBean;
import com.mycompany.bookstorage.dto.BookFilterDTO;
import com.mycompany.bookstorage.entity.BookEntity;
import com.mycompany.bookstorage.mapper.BookMapper;
import com.mycompany.bookstorage.service.BookService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.mapstruct.factory.Mappers;


@WebServlet(name = "BookRegistry", urlPatterns = {"/listBooks"})
public class ListBooksServlet extends HttpServlet {
    
    private static final String LIST_BOOKS_PAGE_BEAN = "listBooksPageBean";
    //private static final BookMapper BOOK_MAPPER = Mappers.getMapper(BookMapper.class);
    
    @Inject
    private BookService bookService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       
        BookFilterDTO filterDTO = new BookFilterDTO();
        filterDTO.setIsbn(req.getParameter("isbn"));
        filterDTO.setTitle(req.getParameter("title"));
        filterDTO.setDescription(req.getParameter("description"));
        filterDTO.setAuthorName(req.getParameter("authorName"));
        
          List<BookEntity> books = bookService.findByFilter(filterDTO);
        ListBooksPageBean listBooksPageBean = (ListBooksPageBean) req.getSession()
                .getAttribute(LIST_BOOKS_PAGE_BEAN);
        
        if (listBooksPageBean == null) {
            listBooksPageBean = new ListBooksPageBean();
            req.getSession().setAttribute(LIST_BOOKS_PAGE_BEAN, listBooksPageBean);
        }
        listBooksPageBean.setBooks(BookMapper.INSTANCE.toDTOList(books));
        
        req.getRequestDispatcher("WEB-INF/listBooks.jsp").forward(req, resp);
       
    }

    
    
    

    
    

}
