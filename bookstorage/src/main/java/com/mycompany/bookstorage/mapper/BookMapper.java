
package com.mycompany.bookstorage.mapper;

import com.mycompany.bookstorage.dto.BookDTO;
import com.mycompany.bookstorage.entity.BookEntity;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;


@Mapper
public interface BookMapper {
    
    BookMapper INSTANCE = Mappers.getMapper(BookMapper.class);
    
    BookDTO toDTO(BookEntity bookEntity);
    
    List<BookDTO> toDTOList(List<BookEntity> bookEntities);
    
}
