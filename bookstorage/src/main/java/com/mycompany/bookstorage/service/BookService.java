
package com.mycompany.bookstorage.service;

import com.mycompany.bookstorage.dao.BookDAO;
import com.mycompany.bookstorage.dto.BookFilterDTO;
import com.mycompany.bookstorage.entity.BookEntity;
import com.mycompany.bookstorage.exeption.WrongBookTitleExeption;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.inject.Inject;



@Singleton
@TransactionAttribute
public class BookService {
    
    @Inject
    private BookDAO bookDAO;
    
    
    
    public List<BookEntity> findAll() {
        return bookDAO.findAll();
    }
    
    public List<BookEntity> findByFilter(BookFilterDTO filterDTO) {
        return bookDAO.findByFilter(filterDTO);
    }
    
    public void save(BookEntity bookEntity) {
        if (bookEntity.getTitle().contains("utca")) {
            throw new WrongBookTitleExeption("Hibás címet adtál meg");
        }
        
        bookDAO.save(bookEntity);
    }
    
    
}
