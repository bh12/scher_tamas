<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>JSP Page</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
              integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
    <body>
        <h1>This is the Library's listed books</h1>
        <form method="get">
            <label for="isbn">ISBN:</label>
            <br>
            <input id="isbn" type="text" name="isbn">
            <br>
            <label for="title">Title:</label>
            <br>
            <input id="title" type="text" name="title">
            <br>
            <label for="description">Description:</label>
            <br>
            <input id="description" type="text" name="description">
            <br>
            <label for="authorName">Author's name:</label>
            <br>
            <input id="authorName" type="text" name="authorName">
            <br>
            <input type="submit" value="Search">
        </form>

        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">ISBN</th>
                    <th scope="col">Title</th>
                    <th scope="col">Description</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${listBooksPageBean.books}" var="book">
                    <tr>
                        <th scope="row">${book.id}</th>
                        <td>${book.isbn}</td>
                        <td>${book.title}</td>
                        <td>${book.description}</td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>

    </body>
</html>
