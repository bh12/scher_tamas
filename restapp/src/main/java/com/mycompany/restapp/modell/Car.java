
package com.mycompany.restapp.modell;


public class Car {
    
    private Long id;
    
    private String brand;
    
    private Engine engine;
    

    public Car(Long id, String brand, Engine engine) {
        this.id = id;
        this.brand = brand;
        this.engine = engine;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
    
    
}
