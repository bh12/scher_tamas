
package com.mycompany.restapp.controller;

import com.mycompany.restapp.modell.Car;
import com.mycompany.restapp.modell.Engine;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/cars")
@Consumes(MediaType.TEXT_PLAIN)
public class CarResources {
    
    @Path("/hello")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String hello() {
        return "Hello!";
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public Response getCars() {
        List<Car> cars = new ArrayList<>();
        cars.add(new Car(1L, "AUDI", new Engine(200)));
        cars.add(new Car(2L, "porshce", new Engine(400)));
        return Response.ok(cars).build();
    }
    
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public Response getCar(@PathParam("id") Long id) {
        return Response.ok(new Car(id, "AUDI", new Engine(200))).build();
        
    }
}
