
package com.mycompany.restapp;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/restapp/api/v1")
public class RestApp extends Application{
    
}
