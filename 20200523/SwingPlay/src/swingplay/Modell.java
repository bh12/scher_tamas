package swingplay;

public class Modell {

    private int indexOfButtonPushed;
    private int indexOfUnvisibleButton;

    public int getIndexOfButtonPushed() {
        return indexOfButtonPushed;
    }

    public void setIndexOfButtonPushed(int indexOfButtonPushed) {
        this.indexOfButtonPushed = indexOfButtonPushed;
    }

    public int getIndexOfUnvisibleButton() {
        return indexOfUnvisibleButton;
    }

    public void setIndexOfUnvisibleButton(int indexOfUnvisibleButton) {
        this.indexOfUnvisibleButton = indexOfUnvisibleButton;
    }

}
