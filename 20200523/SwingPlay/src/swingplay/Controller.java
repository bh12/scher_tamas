package swingplay;

import javax.swing.JButton;

public class Controller {

    private View view;
    private Modell modell;

    public Controller() {
        this.view = new View(this);
        this.view.init();
        this.modell = new Modell();
        //this.modell.init();

    }

    public int rndGenerator(int min, int max) {
        int rnd = (int) (Math.random() * (max - min + 1) - min);
        return rnd;
    }

    public void handleButtonClick(int indexOfButtonPushed) {
        int rndButtonIndex;

        do {
            rndButtonIndex = rndGenerator(0, 8);

        } while (rndButtonIndex == indexOfButtonPushed || rndButtonIndex == this.modell.getIndexOfUnvisibleButton());

        this.view.buttonsArray[this.modell.getIndexOfUnvisibleButton()].setVisible(true);
        this.view.buttonsArray[rndButtonIndex].setVisible(false);
        this.modell.setIndexOfUnvisibleButton(rndButtonIndex);
    }

}
