package swingplay;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class View extends JFrame implements ActionListener {

    private static final int BUTTON_NUMBRES = 9;

    private Controller controller;
    private JFrame frame;
    public static JButton[] buttonsArray = new JButton[BUTTON_NUMBRES];

    public View(Controller controller) {
        this.controller = controller;
    }

    public void init() {

        this.setSize(400, 400);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setTitle("Gomb eltüntetés");

        for (int i = 0; i < BUTTON_NUMBRES; i++) {
            JButton button = new JButton();
            button.setBackground(new Color(666666666 * i));
            Border border = new LineBorder(Color.WHITE);
            button.setBorder(border);
            add(button);
            this.buttonsArray[i] = button;

        }

        this.setLayout(new GridLayout(3, 3));

        updateView();

        this.setVisible(true);

    }

    public void updateView() {

        for (int i = 0; i < buttonsArray.length; i++) {

            buttonsArray[i].addActionListener(this);
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        int indexOfButtonPushed = -1;
        for (int i = 0; i < buttonsArray.length; i++) {
            if (ae.getSource() == buttonsArray[i]) {
                indexOfButtonPushed = i;
            }

        }
        controller.handleButtonClick(indexOfButtonPushed);

    }

}
